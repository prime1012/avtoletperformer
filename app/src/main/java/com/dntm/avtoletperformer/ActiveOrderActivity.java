package com.dntm.avtoletperformer;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActiveOrderActivity extends AppCompatActivity {
    ProgressDialog pd;
    JSONObject routeData, info, client;
    JSONArray performers;
    BroadcastReceiver orderCanceled, statusPerformerChanged;
    Integer status, orderId;
    Map<Integer, String> statusList;
    PerformerAdapter performerAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_order);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Активный заказ");
        toolbar.setNavigationIcon(R.drawable.ic_close);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Context context = this;
        final Intent intent = getIntent();

        status = Integer.valueOf(Config.getInfoPerformer(this,"status").toString());

        if(status > 3)
            ((FloatingActionButton) findViewById(R.id.cancelOrder)).setVisibility(View.GONE);

        statusList = new HashMap<Integer, String>();
        statusList.put(2, "По адресу");
        statusList.put(3, "Начать работу");
        statusList.put(4, "Закончить работу");

        ((TextView) findViewById(R.id.seekBarStatusText)).setText(statusList.get(status));

        if(getSharedPreferences("activeOrder", 0).getInt("orderId", -1) != -1) {
            try {
                SharedPreferences pref = getSharedPreferences("activeOrder", 0);
                orderId = pref.getInt("orderId", -1);
                routeData = new JSONObject(pref.getString("routeData", ""));
                performers = new JSONArray(pref.getString("performers", ""));
                info = new JSONObject(pref.getString("info", ""));
                client = new JSONObject(pref.getString("client", ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            ((TextView) findViewById(R.id.originOrder)).setText(routeData.getString("origin"));
            ((TextView) findViewById(R.id.destinationOrder)).setText(routeData.getString("destination"));
            ((TextView) findViewById(R.id.cargoDescription)).setText(info.getString("cargoDescription"));

            ((TextView) findViewById(R.id.clientName)).setText(client.getString("name"));
            ((TextView) findViewById(R.id.clientRating)).setText(client.getString("rating"));

            if(!info.isNull("services") && Config.getInfoPerformer(this,"type").equals(0)) {
                String services = info.getString("services");
                ((LinearLayout) findViewById(R.id.servicesOrder)).setVisibility(View.VISIBLE);

                if(services.contains("1") && !services.contains("2"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Вывоз мусора");
                else if(services.contains("2") && !services.contains("1"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Экспедирование");
                else if(services.contains("1") && services.contains("2"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Вывоз мусора, экспедирование");
            }

            performerAdapter = new PerformerAdapter(ActiveOrderActivity.this, R.layout.performer_list_item);
            List<Performer> performerList = new ArrayList<Performer>();
            LinearLayout performersListLabel = (LinearLayout) findViewById(R.id.performersListLabel);

            for (int i = 0; i < performers.length(); i++) {
                JSONObject performer = performers.getJSONObject(i);

                if(!performer.isNull("performer")) {
                    JSONObject profile = performer.getJSONObject("performer");
                    if(!Config.getInfoPerformer(this,"id").equals(profile.getInt("id"))) {
                        if(performersListLabel.getVisibility() == View.GONE)
                            performersListLabel.setVisibility(View.VISIBLE);

                        performerList.add(new Performer(
                                profile.getInt("id"),
                                performer.getInt("type"),
                                profile.getJSONObject("full_name").getString("name"),
                                profile.getString("phoneNumber"),
                                profile.getDouble("rating"),
                                profile.getInt("status")
                        ));
                    }
                }
                else {
                    if(performersListLabel.getVisibility() == View.GONE)
                        performersListLabel.setVisibility(View.VISIBLE);
                    performerList.add(null);
                }
            }

            performerAdapter.addAll(performerList);
            performerAdapter.notifyDataSetChanged();

            LinearLayout layout = (LinearLayout) findViewById(R.id.performersList);

            final int adapterCount = performerAdapter.getCount();

            for (int i = 0; i < adapterCount; i++) {
                View item = performerAdapter.getView(i, null, null);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                item.setLayoutParams(lp);
                layout.addView(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        orderCanceled = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getSharedPreferences("activeOrder", 0).edit().clear().apply();
                finish();
            }
        };

        statusPerformerChanged = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    JSONObject payload = new JSONObject(intent.getStringExtra("payload"));
                    JSONArray performers = payload.getJSONArray("performers");

                    List<Performer> performerList = new ArrayList<Performer>();
                    for (int i = 0; i < performers.length(); i++) {
                        JSONObject performer = performers.getJSONObject(i);
                        if(!performer.isNull("performer")) {
                            JSONObject profile = performer.getJSONObject("performer");
                            if(!Config.getInfoPerformer(ActiveOrderActivity.this,"id").equals(profile.getInt("id"))) {
                                performerList.add(new Performer(
                                        profile.getInt("id"),
                                        performer.getInt("type"),
                                        profile.getJSONObject("full_name").getString("name"),
                                        profile.getString("phoneNumber"),
                                        profile.getDouble("rating"),
                                        profile.getInt("status")
                                ));
                            }
                        }
                        else
                            performerList.add(null);
                    }

                    if(performerAdapter.getCount() > 0)
                        performerAdapter.clear();

                    performerAdapter.addAll(performerList);
                    performerAdapter.notifyDataSetChanged();

                    LinearLayout layout = (LinearLayout) findViewById(R.id.performersList);
                    layout.removeAllViews();

                    final int adapterCount = performerAdapter.getCount();

                    for (int i = 0; i < adapterCount; i++) {
                        View item = performerAdapter.getView(i, null, null);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                        item.setLayoutParams(lp);
                        layout.addView(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(orderCanceled, new IntentFilter(Config.PUSH_ORDER_CANCELED));
        LocalBroadcastManager.getInstance(this).registerReceiver(statusPerformerChanged, new IntentFilter(Config.PUSH_PERFORMERS_STATUS_CHANGED));

        ((Button) findViewById(R.id.clientChat)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent chat = new Intent(ActiveOrderActivity.this, MessageListActivity.class);
                    chat.putExtra("name", client.getString("name"));
                    chat.putExtra("phoneNumber", client.getString("phoneNumber"));
                    chat.putExtra("userId", client.getInt("id"));
                    startActivity(chat);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //Обновление статуса водителя во время заказа
        ((SeekBar) findViewById(R.id.seekBarStatus)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBar.getProgress() != 100) {
                    seekBar.setProgress(0);
                } else {
                    final Integer updatedStatus = status + 1;

                    pd = new ProgressDialog(ActiveOrderActivity.this);
                    pd.setMessage("Пожалуйста, подождите");
                    pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pd.setIndeterminate(true);
                    pd.show();

                    if(updatedStatus != 5)
                        ((TextView) findViewById(R.id.seekBarStatusText)).setText(statusList.get(updatedStatus));

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                            Config.API_SET_STATUS + "?code=" + updatedStatus + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                            new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        if (pd != null)
                                            pd.dismiss();
                                        Log.d("SetStatusResponse", response.toString());
                                        try {
                                            Toast.makeText(context, String.format("Ваш статус изменён на «%s»", response.getString("text")), Toast.LENGTH_SHORT).show();
                                            Config.setInfoPerformer(ActiveOrderActivity.this,"status", response.getInt("id"));
                                            status = response.getInt("id");

                                            if(status > 3)
                                                ((FloatingActionButton) findViewById(R.id.cancelOrder)).setVisibility(View.GONE);

                                            //Если у водителя статус "Освобождаюсь", то надо завершить заказ
                                            if(status == 5) {
                                                getSharedPreferences("activeOrder", 0)
                                                        .edit()
                                                        .putInt("status", 1)
                                                        .putString("fare", response.getJSONArray("fare").toString())
                                                        .putInt("totalPrice", response.getInt("totalPrice"))
                                                        .apply();
                                                getSharedPreferences("messagesClient", 0).edit().clear().apply();

                                                Intent activity = new Intent(context, FinishOrderActivity.class);
                                                startActivity(activity);
                                                finish();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (pd != null)
                                    pd.dismiss();
                                Toast.makeText(ActiveOrderActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                                Log.d("SetStatusResponse", error + "");
                            }
                    });
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                            0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                    requestQueue.add(jsonObjectRequest);

                    seekBar.setProgress(0);
                }
            }
        });

        ((FloatingActionButton) findViewById(R.id.cancelOrder)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Причина отмены заказа")
                        .setCancelable(false)
                        .setItems(R.array.reasonCancelOrder, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String reason = "";
                                try {
                                    reason = URLEncoder.encode(getResources().getStringArray(R.array.reasonCancelOrder)[which], "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                                String requestUrl = Config.API_CANCEL_ORDER
                                        + "?orderId=" + getSharedPreferences("activeOrder", 0).getInt("orderId", -1)
                                        + "&reason=" + reason
                                        + "&token=" + Config.getAuthToken(context);

                                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                                        requestUrl, null,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                Log.d("CancelOrderResponse", response.toString());
                                                try {
                                                    if(response.getString("status").contains("error")) {
                                                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                                        builder.setTitle("Уведомление")
                                                                .setMessage(response.getString("errorMsg"))
                                                                .setCancelable(false)
                                                                .setNegativeButton("ОК",
                                                                        new DialogInterface.OnClickListener() {
                                                                            public void onClick(DialogInterface dialog, int id) {
                                                                                dialog.cancel();
                                                                            }
                                                                        });
                                                        AlertDialog alert = builder.create();
                                                        alert.show();
                                                    } else if(response.getString("status").contains("success")) {
                                                        //Удаление информации о заказе в приложение
                                                        getSharedPreferences("activeOrder", 0).edit().clear().apply();
                                                        finish();
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                                        Log.d("CancelOrderResponse", error + "");
                                    }
                                });
                                RequestQueue requestQueue = Volley.newRequestQueue(context);
                                requestQueue.add(jsonObjectRequest);
                            }
                        })
                        .setNegativeButton("Назад",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(orderCanceled);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(statusPerformerChanged);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_navigator:
                try {
                    // Создаем интент для построения маршрута
                    Intent intent = new Intent("ru.yandex.yandexnavi.action.BUILD_ROUTE_ON_MAP");
                    intent.setPackage("ru.yandex.yandexnavi");

                    PackageManager pm = getPackageManager();
                    List<ResolveInfo> infos = pm.queryIntentActivities(intent, 0);

                    // Проверяем, установлен ли Яндекс.Навигатор
                    if (infos == null || infos.size() == 0) {
                        // Если нет - будем открывать страничку Навигатора в Google Play
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=ru.yandex.yandexnavi"));
                    } else {
                        intent.putExtra("lat_from", routeData.getJSONObject("originLocation").getDouble("lat"));
                        intent.putExtra("lon_from", routeData.getJSONObject("originLocation").getDouble("lng"));
                        intent.putExtra("lat_to", routeData.getJSONObject("destinationLocation").getDouble("lat"));
                        intent.putExtra("lon_to", routeData.getJSONObject("destinationLocation").getDouble("lng"));
                    }

                    // Запускаем нужную Activity
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
        return true;
    }
}
