package com.dntm.avtoletperformer;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    private static Context context;
    String phoneNumber;
    boolean phoneNumberFilled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;

        final EditText phoneNumberText = (EditText) findViewById(R.id.phoneNumberLogin);
        final TextView infoLogin = (TextView) findViewById(R.id.infoLogin);
        final Button nextLogin = (Button)findViewById(R.id.nextLogin);
        infoLogin.setMovementMethod(LinkMovementMethod.getInstance());

        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                "+7 ([000]) [000]-[00]-[00]",
                false,
                phoneNumberText,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        phoneNumber = extractedValue;
                        phoneNumberFilled = maskFilled;
                    }
                }
        );

        phoneNumberText.addTextChangedListener(listener);
        phoneNumberText.setOnFocusChangeListener(listener);
        phoneNumberText.setHint(listener.placeholder());

        //Отправка код на указанный номер телефона
        nextLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phoneNumberFilled) {
                    try {
                        String requestUrl = Config.API_REQUEST_SMS_CODE + "?phone_number=7" + phoneNumber;
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                                requestUrl, null,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d("LoginResponse", response.toString());
                                        try {
                                            if(response.getString("status").toString().contains("success")) {
                                                Intent intent = new Intent(context, VerifyLoginActivity.class);
                                                intent.putExtra("phoneNumber", "7" + phoneNumber);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                                finish();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                                Log.d("LoginResponseError", error + "");
                            }
                        });
                        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                                0,
                                0,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(jsonObjectRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
