package com.dntm.avtoletperformer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CitySelectorActivity extends AppCompatActivity {

    ProgressDialog pd;
    List<Place> settlementsList = new ArrayList<>();
    PlaceAdapter settlementsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_city_selector);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Поиск города");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final ListView placesListView = (ListView) findViewById(R.id.placesList);
        settlementsListAdapter = new PlaceAdapter(this, R.layout.place_list_item);
        placesListView.setAdapter(settlementsListAdapter);

        pd = new ProgressDialog(this);
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Config.API_GET_SETTLEMENTS + "?token=" + Config.getAuthToken(this), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (pd != null)
                            pd.dismiss();
                        try {
                            Log.d("GetSettlementsResponse", response.toString());
                            JSONArray settlements = response.getJSONArray("settlements");

                            for(int i = 0; i < settlements.length(); i++) {
                                Place settlement = new Place(
                                        settlements.getJSONObject(i).getString("id"),
                                        settlements.getJSONObject(i).getString("name"),
                                        settlements.getJSONObject(i).getString("name")
                                );
                                settlementsList.add(settlement);
                            }

                            settlementsListAdapter.clear();
                            settlementsListAdapter.addAll(settlementsList);
                            settlementsListAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                Toast.makeText(CitySelectorActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("GetSettlementsResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

        final EditText placeQuery = (EditText) findViewById(R.id.placeQuery);
        placeQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<Place> settlementsListFiltered = new ArrayList<>();
                settlementsListAdapter.clear();

                if(charSequence.length() != 0) {
                    if(charSequence.charAt(charSequence.length() - 1) != ' ') {
                        for(int i3 = 0; i3 < settlementsList.size(); i3++) {
                            Place settlement = settlementsList.get(i3);
                            if(settlement.getPrimaryText().length() >= charSequence.length())
                                if(settlement.getPrimaryText().toLowerCase().substring(0, charSequence.length()).contains(charSequence.toString().toLowerCase()))
                                    settlementsListFiltered.add(settlement);
                        }

                        settlementsListAdapter.addAll(settlementsListFiltered);
                        settlementsListAdapter.notifyDataSetChanged();
                        return;
                    }
                }

                settlementsListAdapter.addAll(settlementsList);
                settlementsListAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        placesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Place selectedPlace = settlementsListAdapter.getItem(i);

                Intent intent = new Intent();
                intent.putExtra("id", selectedPlace.getId());
                intent.putExtra("name", selectedPlace.getPrimaryText());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
