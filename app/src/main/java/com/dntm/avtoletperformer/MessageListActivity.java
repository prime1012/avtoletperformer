package com.dntm.avtoletperformer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MessageListActivity extends AppCompatActivity {
    String phoneNumber;
    Integer userId;
    LinearLayoutManager mRecyclerManager;
    RecyclerView mMessageRecycler;
    MessageListAdapter mMessageAdapter;
    List<Message> messagesList = new ArrayList<Message>();
    SharedPreferences messagesClient;
    JSONArray messagesJsonArray = new JSONArray();
    BroadcastReceiver newMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Button button_chatbox_send = (Button) findViewById(R.id.button_chatbox_send);
        final EditText edittext_chatbox = (EditText) findViewById(R.id.edittext_chatbox);

        Intent intent = getIntent();
        setTitle(intent.getStringExtra("name"));
        phoneNumber = intent.getStringExtra("phoneNumber");
        userId = intent.getIntExtra("userId", -1);

        //Получение списка сообщений
        updateMessagesList();
        try {
            for (int i = 0; i < messagesJsonArray.length(); i++) {
                JSONObject message = messagesJsonArray.getJSONObject(i);
                messagesList.add(new Message(
                        message.getInt("userId"),
                        message.getString("text"),
                        message.getLong("timestamp"))
                );
            }

            mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
            mMessageAdapter = new MessageListAdapter(this, messagesList);
            mMessageRecycler.setAdapter(mMessageAdapter);

            mRecyclerManager = new LinearLayoutManager(this);
            mRecyclerManager.setReverseLayout(false);
            mMessageRecycler.setLayoutManager(mRecyclerManager);
            mRecyclerManager.smoothScrollToPosition(mMessageRecycler, null, mMessageAdapter.getItemCount());

            mMessageRecycler.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                    mRecyclerManager.smoothScrollToPosition(mMessageRecycler, null, mMessageAdapter.getItemCount());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        button_chatbox_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Добавление сообщение в чат
                Calendar calendar = Calendar.getInstance();
                final Message sentMessage = new Message(
                        Integer.parseInt(Config.getInfoPerformer(MessageListActivity.this, "id").toString()),
                        edittext_chatbox.getText().toString(),
                        calendar.getTimeInMillis() / 1000
                );
                messagesList.add(sentMessage);
                mMessageAdapter.notifyDataSetChanged();
                edittext_chatbox.setText("");

                String textMessage = null;
                try {
                    textMessage = URLEncoder.encode(sentMessage.getText(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                //Отправка сообщение
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                        Config.API_SEND_MESSAGE
                                + "?clientId=" + userId
                                + "&text=" + textMessage
                                + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("SendMessageResponse", response.toString());
                                try {
                                    //Добавление сообщение в хранилище 'messagesClient'
                                    updateMessagesList();
                                    JSONObject message = new JSONObject();
                                    message.put("userId", sentMessage.getUserId());
                                    message.put("text", sentMessage.getText());
                                    message.put("timestamp", sentMessage.getTimestamp());
                                    messagesJsonArray.put(message);
                                    messagesClient.edit().putString("messages", messagesJsonArray.toString()).apply();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                messagesList.remove(sentMessage);
                                mMessageAdapter.notifyDataSetChanged();
                                Log.d("SendMessageResponse", error + "");
                            }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                requestQueue.add(jsonObjectRequest);
            }
        });

        newMessage = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    JSONObject payload = new JSONObject(intent.getStringExtra("payload"));

                    final Message newMessage = new Message(
                            payload.getInt("userId"),
                            payload.getString("text"),
                            payload.getLong("timestamp")
                    );
                    messagesList.add(newMessage);
                    mMessageAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(newMessage, new IntentFilter(Config.PUSH_NEW_MESSAGE));
    }

    private void updateMessagesList() {
        messagesClient = getSharedPreferences("messagesClient", MODE_PRIVATE);

        try {
            if(messagesClient.getString("messages", null) != null)
                messagesJsonArray = new JSONArray(messagesClient.getString("messages", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(newMessage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.phone, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_phone:
                if (ActivityCompat.checkSelfPermission(MessageListActivity.this, android.Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:+" + phoneNumber));
                    startActivity(callIntent);
                } else
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 0);
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:+" + phoneNumber));
                startActivity(callIntent);
            }
        }
    }
}
