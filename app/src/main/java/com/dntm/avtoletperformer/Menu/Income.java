package com.dntm.avtoletperformer.Menu;


public class Income {
    private int numberOfOrders;
    private String strTime;
    private int amount;

    Income(String strTime, int numberOfOrders, int amount) {
        this.numberOfOrders = numberOfOrders;
        this.strTime = strTime;
        this.amount = amount;
    }

    public void addAmount(int amount) {
        this.amount += amount;
    }
    public void addNumberOfOrders(int numberOfOrders) {
        this.numberOfOrders += numberOfOrders;
    }

    public int getNumberOfOrders() {
        return this.numberOfOrders;
    }
    public int getAmount() {
        return this.amount;
    }
    public String getStrTime() {
        return this.strTime;
    }
}
