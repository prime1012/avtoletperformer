package com.dntm.avtoletperformer.Menu;


public class Transaction {
    private int type;
    private String description;
    private double amount;
    private long timestamp;

    Transaction(int type, String description, double amount, long timestamp) {
        this.type = type;
        this.description = description;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public int getType() {
        return this.type;
    }
    public String getDescription() {
        return this.description;
    }
    public double getAmount() {
        return this.amount;
    }
    public long getTimestamp() {
        return this.timestamp;
    }
}
