package com.dntm.avtoletperformer.Menu;


import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dntm.avtoletperformer.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class TransactionAdapter extends ArrayAdapter<Transaction> {

    public TransactionAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public TransactionAdapter(Context context, int resource, List<Transaction> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item, null);
        }

        Transaction t = getItem(position);

        if (t != null) {
            TextView descriptionTransaction = (TextView) v.findViewById(R.id.firstText);
            TextView amountTransaction = (TextView) v.findViewById(R.id.thirdText);
            TextView timestampTransaction = (TextView) v.findViewById(R.id.secondText);

            descriptionTransaction.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            amountTransaction.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            timestampTransaction.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));

            if (descriptionTransaction != null)
                descriptionTransaction.setText(t.getDescription());

            if (amountTransaction != null)
                amountTransaction.setText((t.getType() == 1 ? "+" : "-") + String.valueOf(t.getAmount()) + " \u20BD");

            if (timestampTransaction != null)
                timestampTransaction.setText(getDateCurrentTimeZone(t.getTimestamp()));
        }

        return v;
    }

    private String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("UTC");
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) { }
        return "";
    }
}
