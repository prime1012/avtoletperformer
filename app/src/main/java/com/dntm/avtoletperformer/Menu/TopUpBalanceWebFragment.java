package com.dntm.avtoletperformer.Menu;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dntm.avtoletperformer.AppInterfaces;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopUpBalanceWebFragment extends Fragment {
    ProgressDialog pd;
    AppInterfaces.onPayment onPayment;

    public TopUpBalanceWebFragment() {
        // Required empty public constructor
    }

    public static TopUpBalanceWebFragment newInstance(String formUrl) {
        TopUpBalanceWebFragment fragment = new TopUpBalanceWebFragment();

        Bundle args = new Bundle();
        args.putString("formUrl", formUrl);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_top_up_balance_web, container, false);

        pd = new ProgressDialog(getActivity());

        WebView payment = (WebView) v.findViewById(R.id.paymentWebView);
        payment.getSettings().setJavaScriptEnabled(true);
        payment.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        payment.clearCache(true);
        payment.clearHistory();
        payment.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                if(newProgress == 100 && view.getUrl().contains(Config.API_PAYMENT_FINISHED))
                    onPayment.onPaymentFinished();
            }
        });
        payment.setWebViewClient(new WebViewClient());
        payment.loadUrl(getArguments().getString("formUrl"));

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onPayment = (AppInterfaces.onPayment) activity;
        } catch (ClassCastException e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
