package com.dntm.avtoletperformer.Menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dntm.avtoletperformer.AppInterfaces;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;
import com.dntm.avtoletperformer.AppInterfaces.onPayment;

import java.util.Locale;

public class TopUpBalanceFormFragment extends Fragment {
    Activity mActivity;
    AppInterfaces.onPayment onPayment;
    int amountNumber, minAmount;

    public TopUpBalanceFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_top_up_balance_form, container, false);

        final EditText amountText = (EditText) v.findViewById(R.id.topUpAmount);
        final TextView amountWithCommissionText = (TextView) v.findViewById(R.id.amountWithCommission);
        final TextView totalBalanceText = (TextView) v.findViewById(R.id.totalBalance);

        amountText.setFilters(new InputFilter[] { new MinMaxFilter("1", "10000") });
        amountText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    amountText.clearFocus();
                    InputMethodManager inputManager = (InputMethodManager)
                            mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.toggleSoftInput(0, 0);
                }
                return false;
            }
        });

        if(Config.getInfoPerformer(mActivity,"type").equals(0)) {
            //amountText.setHint(String.valueOf(Config.ACQUIRING_MIN_AMOUNT_DRIVER));
            minAmount = Config.ACQUIRING_MIN_AMOUNT_DRIVER;
        }
        else if(Config.getInfoPerformer(mActivity,"type").equals(1)) {
            //amountText.setHint(String.valueOf(Config.ACQUIRING_MIN_AMOUNT_LOADER));
            minAmount = Config.ACQUIRING_MIN_AMOUNT_LOADER;
        }

        amountText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() != 0) {
                    amountNumber = Integer.parseInt(charSequence.toString());
                    double amountWithCommission = amountNumber - (amountNumber * Config.ACQUIRING_PERCENTAGE);
                    int totalBalance = Integer.parseInt(Config.getInfoPerformer(mActivity,"balance").toString()) + (int) amountWithCommission;
                    amountWithCommissionText.setText(String.valueOf(amountWithCommission) + " \u20BD");
                    totalBalanceText.setText(String.valueOf(totalBalance) + " \u20BD");
                } else {
                    amountWithCommissionText.setText("0 \u20BD");
                    totalBalanceText.setText("0 \u20BD");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        ((Button) v.findViewById(R.id.createPaymentButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(amountNumber < minAmount || amountNumber == 0)
                    Toast.makeText(mActivity, String.format(Locale.US,"Минимальная сумма пополнения %d \u20BD", minAmount), Toast.LENGTH_SHORT).show();
                else
                    onPayment.onCreatePayment(amountNumber);
            }
        });

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
        
        try {
            onPayment = (AppInterfaces.onPayment) activity;
        } catch (ClassCastException e) {

        }
    }

    public class MinMaxFilter implements InputFilter {

        private int mIntMin, mIntMax;

        public MinMaxFilter(int minValue, int maxValue) {
            this.mIntMin = minValue;
            this.mIntMax = maxValue;
        }

        public MinMaxFilter(String minValue, String maxValue) {
            this.mIntMin = Integer.parseInt(minValue);
            this.mIntMax = Integer.parseInt(maxValue);
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            try {
                int input = Integer.parseInt(dest.toString() + source.toString());
                if (isInRange(mIntMin, mIntMax, input))
                    return null;
            } catch (NumberFormatException nfe) { }
            return "";
        }

        private boolean isInRange(int a, int b, int c) {
            return b > a ? c >= a && c <= b : c >= b && c <= a;
        }
    }
}
