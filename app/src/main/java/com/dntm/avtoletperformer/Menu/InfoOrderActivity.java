package com.dntm.avtoletperformer.Menu;

import android.app.ProgressDialog;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.Fare;
import com.dntm.avtoletperformer.FareAdapter;
import com.dntm.avtoletperformer.Performer;
import com.dntm.avtoletperformer.PerformerAdapter;
import com.dntm.avtoletperformer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class InfoOrderActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_order);

        Integer orderId = getIntent().getIntExtra("orderId", 0);
        final Boolean orderCanceled = getIntent().getBooleanExtra("orderCanceled", true);

        if(!orderCanceled)
            ((LinearLayout) findViewById(R.id.otherOrderInfo)).setVisibility(View.VISIBLE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.setTitle("Заказ №" + orderId);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        pd = new ProgressDialog(InfoOrderActivity.this);
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        final String requestUrl = Config.API_GET_ORDER_INFO
                + "?orderId=" + orderId
                + "&token=" + Config.getAuthToken(InfoOrderActivity.this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("GetOrderInfoResponse", response.toString());
                        try {
                            if (pd != null)
                                pd.dismiss();

                            //Показ только информации о маршруте, если заказ отменён
                            if(orderCanceled) {
                                JSONObject info = response.getJSONObject("info");

                                //Информация маршрута
                                ((TextView) findViewById(R.id.originOrder)).setText(info.getString("origin"));
                                ((TextView) findViewById(R.id.destinationOrder)).setText(info.getString("destination"));
                                ((TextView) findViewById(R.id.cargoDescription)).setText(info.getString("cargoDescription"));
                                ((LinearLayout) findViewById(R.id.reasonCancel)).setVisibility(View.VISIBLE);
                                ((TextView) findViewById(R.id.reasonCancelText)).setText(info.getString("reason"));

                                return;
                            }

                            JSONObject info = response.getJSONObject("info");
                            JSONArray performers = info.getJSONArray("performers");

                            //Информация о маршруте
                            ((TextView) findViewById(R.id.originOrder)).setText(info.getString("origin"));
                            ((TextView) findViewById(R.id.destinationOrder)).setText(info.getString("destination"));
                            ((TextView) findViewById(R.id.cargoDescription)).setText(info.getString("cargoDescription"));

                            //Добавление коллег
                            final LinearLayout performersListLabel = (LinearLayout) findViewById(R.id.performersListLabel);
                            final PerformerAdapter performerAdapter = new PerformerAdapter(InfoOrderActivity.this, R.layout.performer_list_item);
                            List<Performer> performerList = new ArrayList<Performer>();

                            for (int i = 0; i < performers.length(); i++) {
                                JSONObject performer = performers.getJSONObject(i);
                                if(!performer.isNull("performer")) {
                                    JSONObject profile = performer.getJSONObject("performer");
                                    if(!Config.getInfoPerformer(InfoOrderActivity.this, "id").equals(profile.getInt("id"))) {
                                        if(performersListLabel.getVisibility() == View.GONE)
                                            performersListLabel.setVisibility(View.VISIBLE);

                                        performerList.add(new Performer(
                                                profile.getInt("id"),
                                                performer.getInt("type"),
                                                profile.getJSONObject("full_name").getString("name"),
                                                profile.getString("phoneNumber"),
                                                profile.getDouble("rating"),
                                                profile.getInt("status")
                                        ));
                                    }
                                }
                            }

                            performerAdapter.addAll(performerList);
                            performerAdapter.notifyDataSetChanged();

                            LinearLayout performersList = (LinearLayout) findViewById(R.id.performersList);

                            final int performerAdapterCount = performerAdapter.getCount();

                            for (int i = 0; i < performerAdapterCount; i++) {
                                View item = performerAdapter.getView(i, null, null);
                                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics()));
                                item.setLayoutParams(lp);
                                performersList.addView(item);
                            }

                            //Информация о комиссии
                            ((TextView) findViewById(R.id.commission)).setText(info.getInt("commission") + " \u20BD");

                            //Информация о стоимости
                            JSONArray fare = info.getJSONArray("fare");
                            ((TextView) findViewById(R.id.totalFeeText)).setText(info.getInt("totalFee") + " \u20BD");

                            FareAdapter fareAdapter = new FareAdapter(InfoOrderActivity.this, R.layout.fare_list_item);
                            List<Fare> fareList = new ArrayList<Fare>();

                            for (int i = 0; i < fare.length(); i++) {
                                JSONObject f = fare.getJSONObject(i);
                                fareList.add(new Fare(
                                    f.getString("name"),
                                    f.getInt("minPrice"),
                                    f.getInt("minTime"),
                                    f.getInt("overTimePrice"),
                                    f.getInt("overTime"),
                                    f.getInt("intercityPrice"),
                                    f.getInt("intercityDistance"),
                                    f.getInt("highDemandAreaPrice"),
                                    f.getInt("highDemandTimePrice")
                                ));
                            }

                            fareAdapter.addAll(fareList);
                            fareAdapter.notifyDataSetChanged();

                            LinearLayout detailFare = (LinearLayout) findViewById(R.id.detailFare);

                            final int fareAdapterCount = fareAdapter.getCount();

                            for (int i = 0; i < fareAdapterCount; i++) {
                                View item = fareAdapter.getView(i, null, null);
                                if(i != fareAdapterCount - 1) {
                                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                    lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                                    item.setLayoutParams(lp);
                                }
                                detailFare.addView(item);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                Toast.makeText(InfoOrderActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("GetOrderInfoResponse", error + "");
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(InfoOrderActivity.this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
