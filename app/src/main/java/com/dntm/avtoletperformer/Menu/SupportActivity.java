package com.dntm.avtoletperformer.Menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;

import com.dntm.avtoletperformer.JivoSite.*;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;

public class SupportActivity extends AppCompatActivity implements JivoDelegate {
    ProgressDialog pd;
    Context context;
    JivoSdk jivoSdk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setTitle("Служба поддержки");
        toolbar.setNavigationIcon(R.drawable.ic_close);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Создание окна чата
        jivoSdk = new JivoSdk((WebView) findViewById(R.id.window_chat), "ru");
        jivoSdk.setUserData(Config.getInfoPerformer(this,"name").toString(), "", Config.getInfoPerformer(this,"phoneNumber").toString(), "Исполнитель");
        jivoSdk.delegate = this;
        jivoSdk.prepare();
    }

    @Override
    public void onEvent(String name, String data) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.phone, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_phone:
                if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.CALL_PHONE)
                        == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(Config.SUPPORT_PHONE_NUMBER));
                    startActivity(callIntent);
                } else
                    requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 0);
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(Config.SUPPORT_PHONE_NUMBER));
                startActivity(callIntent);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
