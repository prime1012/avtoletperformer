package com.dntm.avtoletperformer.Menu;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class FinanceFragment extends Fragment {
    BarChart financeChart;
    IncomeAdapter incomeAdapter;
    List<BarEntry> chartPerWeek = new ArrayList<>();
    List<BarEntry> chartPerMonth = new ArrayList<>();
    List<Income> incomePerWeek = new ArrayList<>();
    List<Income> incomePerMonth = new ArrayList<>();
    int amountDay = 0;
    int amountWeek = 0;

    public FinanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_finance, container, false);


        financeChart = (BarChart) v.findViewById(R.id.financeChart);
        financeChart.setTouchEnabled(false);
        financeChart.setDragEnabled(false);
        financeChart.setDrawGridBackground(false);
        financeChart.setDrawBarShadow(false);
        financeChart.getLegend().setEnabled(false);
        financeChart.getDescription().setEnabled(false);
        financeChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        financeChart.setNoDataText("Недостаточно информации");
        financeChart.setNoDataTextColor(R.color.colorGrey);
        financeChart.setNoDataTextTypeface(ResourcesCompat.getFont(getActivity(), R.font.cuprum_regular));
        financeChart.getAxisLeft().setEnabled(false);
        financeChart.getAxisRight().setEnabled(false);
        financeChart.setFitBars(true);
        financeChart.setExtraBottomOffset(4f);

        final String[] quarters = new String[] { "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс" };

        IAxisValueFormatter formatter = new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return quarters[(int) value];
            }

        };

        XAxis xAxis = financeChart.getXAxis();
        xAxis.setTypeface(ResourcesCompat.getFont(getActivity(), R.font.cuprum_regular));
        xAxis.setTextSize(getResources().getDimension(R.dimen.text_small));
        xAxis.setTextColor(R.color.colorGrey);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(formatter);

        YAxis leftAxis = financeChart.getAxisLeft();
        leftAxis.setAxisMinValue(0f);

        YAxis rightAxis = financeChart.getAxisRight();
        rightAxis.setAxisMinValue(0f);

        for(int i = 0; i < 7; i++) {
            chartPerWeek.add(new BarEntry((float) i, 0f));
            chartPerMonth.add(new BarEntry((float) i, 0f));
        }

        RadioGroup financePeriod = (RadioGroup) v.findViewById(R.id.financePeriod);
        financePeriod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(radioGroup.getCheckedRadioButtonId() == R.id.financePerWeek) {
                    ((TextView) v.findViewById(R.id.financeAmount)).setText(amountDay + " \u20BD");
                    ((TextView) v.findViewById(R.id.financePeriodText)).setText(R.string.finance_per_day);
                    ((TextView) v.findViewById(R.id.incomePeriodText)).setText(R.string.income_per_week);
                    updateListView(incomePerWeek);
                    updateChart(chartPerWeek);
                }
                else {
                    ((TextView) v.findViewById(R.id.financeAmount)).setText(amountWeek + " \u20BD");
                    ((TextView) v.findViewById(R.id.financePeriodText)).setText(R.string.finance_per_week);
                    ((TextView) v.findViewById(R.id.incomePeriodText)).setText(R.string.income_per_month);
                    updateListView(incomePerMonth);
                    updateChart(chartPerMonth);
                }
            }
        });

        incomeAdapter = new IncomeAdapter(getActivity(), R.layout.list_item);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Config.API_GET_FINANCE + "?token=" + Config.getAuthToken(getActivity().getBaseContext()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("getFinanceResponse", response.toString());
                        try {
                            JSONArray jsonArray = response.getJSONArray("finance");

                            Calendar calendar = Calendar.getInstance();
                            TimeZone tz = TimeZone.getTimeZone("UTC");
                            calendar.set(Calendar.WEEK_OF_YEAR, response.getInt("currentWeek"));
                            calendar.set(Calendar.MONTH, response.getInt("currentMonth"));
                            int todayDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                            String currentMonth = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("ru", "RU"));

                            Calendar currentWeek = Calendar.getInstance();
                            currentWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
                            currentWeek.set(Calendar.WEEK_OF_YEAR, response.getInt("currentWeek"));
                            for (int i = 0; i < 7; i++) {
                                currentWeek.add(Calendar.DATE, i == 0 ? 0 : 1);
                                int currentDay = currentWeek.getTime().getDate();
                                incomePerWeek.add(new Income(currentDay + " " + currentWeek.getDisplayName(Calendar.MONTH, Calendar.LONG, new Locale("ru", "RU")), 0, 0));
                            }

                            for(int i = 0; i < calendar.getActualMaximum(Calendar.WEEK_OF_MONTH) - 1; i++) {
                                Pair<String, String> rangeWeek = getWeekRange(getFirstWeek(calendar.get(Calendar.MONTH)) + i);
                                incomePerMonth.add(new Income(rangeWeek.first + " - " + rangeWeek.second, 0, 0));
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject day = jsonArray.getJSONObject(i);

                                if(day.getInt("weekOfYear") == response.getInt("currentWeek")) {
                                    amountWeek += day.getInt("amount");
                                    chartPerWeek.set(day.getInt("dayOfWeek") - 1, new BarEntry(
                                            day.getInt("dayOfWeek") - 1,
                                            chartPerWeek.get(day.getInt("dayOfWeek") - 1).getY() + (float) day.getInt("amount"))
                                    );
                                    incomePerWeek.get(day.getInt("dayOfWeek") - 1).addAmount(day.getInt("amount"));
                                    incomePerWeek.get(day.getInt("dayOfWeek") - 1).addNumberOfOrders(1);
                                }

                                int weekOfMonth = getWeekOfMonth(day.getInt("weekOfYear")) - 1;
                                incomePerMonth.get(weekOfMonth).addAmount(day.getInt("amount"));
                                incomePerMonth.get(weekOfMonth).addNumberOfOrders(1);
                                chartPerMonth.set(day.getInt("dayOfWeek") - 1, new BarEntry(
                                        day.getInt("dayOfWeek") - 1,
                                        chartPerMonth.get(day.getInt("dayOfWeek") - 1).getY() + (float) day.getInt("amount"))
                                );

                                if(day.getInt("dayOfWeek") == todayDayOfWeek)
                                    amountDay += day.getInt("amount");
                            }
                            ((TextView) v.findViewById(R.id.financeAmount)).setText(amountDay + " \u20BD");
                            updateChart(chartPerWeek);
                            updateListView(incomePerWeek);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("getFinanceResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
        requestQueue.add(jsonObjectRequest);

        return v;
    }

    private Pair<String,String> getWeekRange(int week_no) {

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.WEEK_OF_YEAR, week_no);
        Date monday = cal.getTime();

        cal.add(Calendar.DATE, 6);
        Date sunday = cal.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", new Locale("ru", "RU"));
        return new Pair<String,String>(sdf.format(monday), sdf.format(sunday));
    }

    private int getFirstWeek(int month_no) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, month_no);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    private int getWeekOfMonth(int week_no) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, week_no);
        return cal.get(Calendar.WEEK_OF_MONTH);
    }

    private void updateChart(List<BarEntry> values) {
        final BarDataSet chartDataSet = new BarDataSet(chartPerWeek, "chartDataSet");
        chartDataSet.setColor(getResources().getColor(R.color.colorAccent));

        final BarData chartData = new BarData(chartDataSet);
        chartData.setBarWidth(0.7f);
        chartData.setDrawValues(false);
        financeChart.setData(chartData);
        
        chartDataSet.setValues(values);
        financeChart.getData().notifyDataChanged();
        financeChart.notifyDataSetChanged();
        financeChart.animateY(500);
        financeChart.invalidate();
    }

    private void updateListView(List<Income> values) {
        incomeAdapter.clear();
        incomeAdapter.addAll(values);
        incomeAdapter.notifyDataSetChanged();

        LinearLayout layout = (LinearLayout) getView().findViewById(R.id.incomeList);
        layout.removeAllViews();

        final int adapterCount = incomeAdapter.getCount();

        for (int i = 0; i < adapterCount; i++) {
            View item = incomeAdapter.getView(i, null, null);
            if(i != adapterCount - 1) {
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                item.setLayoutParams(lp);
            }
            layout.addView(item);
        }
    }
}
