package com.dntm.avtoletperformer.Menu;


import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dntm.avtoletperformer.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class IncomeAdapter extends ArrayAdapter<Income> {

    public IncomeAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public IncomeAdapter(Context context, int resource, List<Income> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_item, null);
        }

        Income i = getItem(position);

        if (i != null) {
            TextView dateText = (TextView) v.findViewById(R.id.firstText);
            TextView numberOfOrdersText = (TextView) v.findViewById(R.id.secondText);
            TextView amountText = (TextView) v.findViewById(R.id.thirdText);

            dateText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            numberOfOrdersText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            amountText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));

            if (dateText != null)
                dateText.setText(i.getStrTime());

            if (numberOfOrdersText != null)
                numberOfOrdersText.setText("Количество заказов: " + i.getNumberOfOrders());

            if (amountText != null)
                amountText.setText(i.getAmount() + " \u20BD");
        }

        return v;
    }
}
