package com.dntm.avtoletperformer.Menu;

import android.Manifest;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.ConfirmationOrderActivity;
import com.dntm.avtoletperformer.NavigationActivity;
import com.dntm.avtoletperformer.NewOrder;
import com.dntm.avtoletperformer.NewOrderAdapter;
import com.dntm.avtoletperformer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NewOrdersFragment extends Fragment {
    ProgressDialog pd;
    BroadcastReceiver newOrder;
    List<NewOrder> orderList = new ArrayList<NewOrder>();
    NewOrderAdapter orderAdapter;
    RadioGroup scopeOrders;
    LinearLayout offlineMode;
    ListView listOrders;
    TextView notOrders;

    public NewOrdersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_new_orders, container, false);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        notOrders = (TextView) v.findViewById(R.id.notOrders);
        offlineMode = (LinearLayout) v.findViewById(R.id.offlineMode);
        Button onlineMode = (Button) v.findViewById(R.id.onlineMode);
        listOrders = (ListView) v.findViewById(R.id.listOrders);
        scopeOrders = (RadioGroup) v.findViewById(R.id.scopeOrders);
        orderAdapter = new NewOrderAdapter(getActivity(), R.layout.new_order_list_item);
        listOrders.setAdapter(orderAdapter);

        onlineMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                    ((NavigationActivity) getActivity()).setStatusOnline();

                    listOrders.setVisibility(View.VISIBLE);
                    offlineMode.setVisibility(View.GONE);

                    if (orderAdapter.getCount() == 0)
                        notOrders.setVisibility(View.VISIBLE);
                } else
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            }
        });

        ((NavigationActivity) getActivity()).drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {

            }

            @Override
            public void onDrawerOpened(View view) {

            }

            @Override
            public void onDrawerClosed(View view) {
                checkOnlineMode();
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

        newOrder = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getNewOrders();
            }
        };

        orderAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                if(Config.getInfoPerformer(getActivity(),"status").equals(1)) {
                    if (orderAdapter.getCount() == 0) {
                        listOrders.setVisibility(View.GONE);
                        offlineMode.setVisibility(View.GONE);
                        notOrders.setVisibility(View.VISIBLE);
                    } else {
                        notOrders.setVisibility(View.GONE);
                        offlineMode.setVisibility(View.GONE);
                        listOrders.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    JSONObject routeData = new JSONObject();
                    routeData.put("orderId", orderAdapter.getItem(i).id);
                    routeData.put("origin", orderAdapter.getItem(i).origin);
                    routeData.put("destination", orderAdapter.getItem(i).destination);
                    routeData.put("cargoDescription", orderAdapter.getItem(i).cargoDescription);
                    routeData.put("distanceToOrder", orderAdapter.getItem(i).distanceToOrder);
                    routeData.put("price", orderAdapter.getItem(i).priceOrder);

                    Intent confirmationOrder = new Intent(getActivity(), ConfirmationOrderActivity.class);
                    confirmationOrder.putExtra("routeData", routeData.toString());
                    startActivity(confirmationOrder);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        scopeOrders.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId) {
                filterScopeOrders();
            }
        });


        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(newOrder, new IntentFilter(Config.PUSH_NEW_ORDER));

        return v;
    }

    private void filterScopeOrders() {
        RadioButton radioButton = (RadioButton) getView().findViewById(scopeOrders.getCheckedRadioButtonId());
        int scopeDistanceOrders = Integer.parseInt(radioButton.getTag().toString());

        List<NewOrder> filterOrderList = new ArrayList<NewOrder>();
        for (int i = 0; i < orderList.size(); i++) {
            NewOrder order = orderList.get(i);
            if(order.distanceToOrder <= scopeDistanceOrders) {
                filterOrderList.add(new NewOrder(
                        order.id,
                        order.origin,
                        order.destination,
                        order.cargoDescription,
                        order.distanceToOrder,
                        order.priceOrder)
                );
            }
        }

        Collections.reverse(filterOrderList);
        orderAdapter.clear();
        orderAdapter.addAll(filterOrderList);
        orderAdapter.notifyDataSetChanged();
    }

    private void getNewOrders() {
        orderList.clear();
        final String requestUrl = Config.API_GET_NEW_ORDERS + "?token=" + Config.getAuthToken(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("GetNewOrdersResponse", response.toString());
                        try {
                            if (pd != null)
                                pd.dismiss();
                            JSONArray orders = response.getJSONArray("orders");

                            for (int i = 0; i < orders.length(); i++) {
                                JSONObject order = orders.getJSONObject(i);
                                orderList.add(new NewOrder(
                                        order.getInt("orderId"),
                                        order.getString("origin"),
                                        order.getString("destination"),
                                        order.getString("cargoDescription"),
                                        order.getDouble("distanceToOrder"),
                                        order.getInt("price"))
                                );
                            }

                            Collections.reverse(orderList);
                            filterScopeOrders();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                if(getActivity() != null)
                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("GetNewOrdersResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        if((int) Config.getInfoPerformer(getActivity(),"balance") <= 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Начало работы")
                    .setMessage("Для того чтобы начать принимать новые заказы, необходимо пополнить баланс внутри сервиса. С баланса будет списываться комиссия с каждого выполненного заказа.")
                    .setCancelable(false)
                    .setNegativeButton("Пополнить баланс",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent intent = new Intent(getActivity().getBaseContext(), TopUpBalanceActivity.class);
                                    startActivityForResult(intent, 0);
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }

        checkOnlineMode();
        getNewOrders();
    }

    private void checkOnlineMode() {
        if(Config.getInfoPerformer(getActivity(),"status").equals(0)) {
            listOrders.setVisibility(View.GONE);
            notOrders.setVisibility(View.GONE);
            offlineMode.setVisibility(View.VISIBLE);
        } else {
            listOrders.setVisibility(View.VISIBLE);
            offlineMode.setVisibility(View.GONE);

            if (orderAdapter.getCount() == 0)
                notOrders.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                ((NavigationActivity) getActivity()).setStatusOnline();

                listOrders.setVisibility(View.VISIBLE);
                offlineMode.setVisibility(View.GONE);

                if (orderAdapter.getCount() == 0)
                    notOrders.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(newOrder);
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
