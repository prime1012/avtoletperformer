package com.dntm.avtoletperformer.Menu;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.AppInterfaces;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;

import org.json.JSONObject;

public class TopUpBalanceActivity extends AppCompatActivity implements AppInterfaces.onPayment {
    ProgressDialog pd;
    int mAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topupbalance);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Баланс");
        toolbar.setSubtitle("Пополнение");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        showFragment(new TopUpBalanceFormFragment());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);
        ft.add(R.id.fragmentContainer, fragment);
        ft.commit();
    }

    @Override
    public void onCreatePayment(int amount) {
        mAmount = amount;
        pd = new ProgressDialog(this);
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        String requestUrl = Config.API_CREATE_PAYMENT + "?amount=" + amount + "&token=" + Config.getAuthToken(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("createPaymentResponse", response.toString());
                        try {
                            if (pd != null)
                                pd.dismiss();
                            showFragment(TopUpBalanceWebFragment.newInstance(response.getString("formUrl")));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (pd != null)
                            pd.dismiss();
                        Log.d("createPaymentResponse", error + "");
                    }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onPaymentFinished() {
        Log.d("onPaymentFinished", "RESULT_OK");
        int amountWithCommission = mAmount - (int)(mAmount * Config.ACQUIRING_PERCENTAGE);
        Config.setInfoPerformer(this,"balance", Integer.parseInt(Config.getInfoPerformer(this,"balance").toString()) + amountWithCommission);
        amountWithCommission = 0;
        mAmount = 0;
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
