package com.dntm.avtoletperformer.Menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.AppInterfaces;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class BalanceFragment extends Fragment {
    ProgressDialog pd;
    TransactionAdapter transactionAdapter;

    public BalanceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_balance, container, false);

        ((TextView) v.findViewById(R.id.balanceProfile)).setText(Config.getInfoPerformer(getActivity(),"balance") + " \u20BD");
        ((Button) v.findViewById(R.id.topUpBalanceButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getBaseContext(), TopUpBalanceActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        transactionAdapter = new TransactionAdapter(getActivity().getBaseContext(), R.layout.list_item);
        final ListView transactionsList = (ListView) v.findViewById(R.id.transactionsList);
        final TextView notInfo = (TextView) v.findViewById(R.id.notInfo);
        transactionsList.setAdapter(transactionAdapter);

        transactionAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                if(transactionAdapter.getCount() == 0) {
                    transactionsList.setVisibility(View.GONE);
                    notInfo.setVisibility(View.VISIBLE);
                } else {
                    notInfo.setVisibility(View.GONE);
                    transactionsList.setVisibility(View.VISIBLE);
                }
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        getLastTransactions();
        Log.d("BalanceFragment", "RESULT_OK");
        ((TextView) getView().findViewById(R.id.balanceProfile)).setText(Config.getInfoPerformer(getActivity(),"balance") + " \u20BD");
    }

    private void getLastTransactions() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Config.API_GET_LAST_TRANSACTIONS + "?token=" + Config.getAuthToken(getActivity().getBaseContext()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("getTransactionsResponse", response.toString());
                        if (pd != null)
                            pd.dismiss();
                        try {
                            JSONArray jsonArray = response.getJSONArray("transactions");
                            List<Transaction> transactions = new ArrayList<Transaction>();
                            for(int i = 0; i < jsonArray.length(); i++) {
                                transactions.add(new Transaction(
                                        jsonArray.getJSONObject(i).getInt("type"),
                                        jsonArray.getJSONObject(i).getString("description"),
                                        jsonArray.getJSONObject(i).getDouble("amount"),
                                        jsonArray.getJSONObject(i).getLong("timestamp")
                                ));
                            }

                            transactionAdapter.clear();
                            transactionAdapter.addAll(transactions);
                            transactionAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                Log.d("getTransactionsResponse", error + "");
                if(getActivity() != null)
                    Toast.makeText(getActivity(), "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getBaseContext());
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
