package com.dntm.avtoletperformer.Menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.NavigationActivity;
import com.dntm.avtoletperformer.Order;
import com.dntm.avtoletperformer.OrderAdapter;
import com.dntm.avtoletperformer.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LastOrdersFragment extends Fragment {
    ProgressDialog pd;
    List<Order> completedOrders = new ArrayList<Order>();
    List<Order> canceledOrders = new ArrayList<Order>();

    public Context context;

    public LastOrdersFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_last_orders, container, false);

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        final TextView notInfo = (TextView) v.findViewById(R.id.notInfo);
        final ListView listOrders = (ListView) v.findViewById(R.id.listOrders);
        final OrderAdapter orderAdapter = new OrderAdapter(getActivity(), R.layout.order_list_item);
        listOrders.setAdapter(orderAdapter);

        final String requestUrl = Config.API_GET_LAST_ORDERS + "?token=" + Config.getAuthToken(getActivity());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("GetLastOrdersResponse", response.toString());
                        try {
                            if (pd != null)
                                pd.dismiss();
                            JSONArray completedOrdersArray = response.getJSONArray("completedOrders");
                            JSONArray canceledOrdersArray = response.getJSONArray("canceledOrders");

                            for (int i = 0; i < completedOrdersArray.length(); i++) {
                                JSONObject order = completedOrdersArray.getJSONObject(i);
                                completedOrders.add(new Order(
                                        order.getInt("id"),
                                        false,
                                        order.getLong("startTime"),
                                        order.getLong("endTime"),
                                        order.getDouble("distanceRoute"),
                                        order.getLong("workingDuration"),
                                        order.getInt("totalFee"))
                                );
                            }

                            for (int i = 0; i < canceledOrdersArray.length(); i++) {
                                JSONObject order = canceledOrdersArray.getJSONObject(i);
                                canceledOrders.add(new Order(
                                        order.getInt("id"),
                                        true,
                                        order.getLong("startTime"),
                                        order.getLong("endTime"),
                                        order.getDouble("distanceRoute"),
                                        order.getLong("workingDuration"),
                                        order.getInt("totalFee"))
                                );
                            }

                            orderAdapter.addAll(completedOrders);
                            orderAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                Toast.makeText(context, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("GetLastOrdersResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest);

        ((RadioGroup) v.findViewById(R.id.typeOrders)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                orderAdapter.clear();
                orderAdapter.addAll(radioGroup.getCheckedRadioButtonId() == R.id.completedOrdersType ? completedOrders : canceledOrders);
                orderAdapter.notifyDataSetChanged();
            }
        });

        orderAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                if(orderAdapter.getCount() == 0) {
                    listOrders.setVisibility(View.GONE);
                    notInfo.setVisibility(View.VISIBLE);
                } else {
                    notInfo.setVisibility(View.GONE);
                    listOrders.setVisibility(View.VISIBLE);
                }
            }
        });

        listOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent activity = new Intent(getActivity(), InfoOrderActivity.class);
                activity.putExtra("orderId", orderAdapter.getItem(i).id);
                activity.putExtra("orderCanceled", orderAdapter.getItem(i).isCanceled);
                startActivity(activity);
            }
        });

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
