package com.dntm.avtoletperformer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getIntExtra("mode", 0) == 0)
            setContentView(R.layout.performer_not_validate);
        else
            setContentView(R.layout.performer_blocked);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Проверка статуса доступа к сервису и статус блокировки
        if((boolean) Config.getInfoPerformer(this,"validate") && !Config.getInfoPerformer(this,"status").equals(-1)) {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            startActivity(intent);
            finish();
        }
    }

    public void onExit(View v) {
        Config.setAuthToken(null, this);
        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivity(intent);
        finish();
    }

    public void onCall(View v) {
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(Config.SUPPORT_PHONE_NUMBER));
            startActivity(callIntent);
        }
        else
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},0);
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(Config.SUPPORT_PHONE_NUMBER));
                startActivity(callIntent);
            }
        }
    }
}
