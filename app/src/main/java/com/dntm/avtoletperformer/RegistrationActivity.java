package com.dntm.avtoletperformer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.dntm.avtoletperformer.VolleyMultipartRequest.DataPart;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.MultipartBody;

public class RegistrationActivity extends AppCompatActivity implements AppInterfaces.onRegistration {
    Context context;
    ProgressDialog pd;
    int mode = 0;
    Map<String, String> mParams = new HashMap<>();
    Map<String, DataPart> mFiles = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Регистрация исполнителя");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(getSupportFragmentManager().getBackStackEntryCount() == 0) {
            onExit();
            return true;
        }
        onBackPressed();
        return true;
    }

    @Override
    public void onPersonalCompleted(Map<String, String> params, Map<String, DataPart> files) {
        mParams.putAll(params);
        mFiles.putAll(files);

        if(mode == Config.DRIVER_MODE) {
            setTitle("Автомобиль");
            showFragment(new AutoRegistrationFragment());
        }
        else {
            setTitle("Документы");
            showFragment(DocumentsRegistrationFragment.newInstance(Config.LOADER_MODE));
        }
    }

    @Override
    public void onAutoCompleted(Map<String, String> params, Map<String, DataPart> files) {
        mParams.putAll(params);
        mFiles.putAll(files);
        setTitle("Документы");
        showFragment(DocumentsRegistrationFragment.newInstance(Config.DRIVER_MODE));
    }

    @Override
    public void onDocumentsCompleted(Map<String, DataPart> files) {
        mFiles.putAll(files);

        pd = new ProgressDialog(context);
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();

        final MediaType MEDIA_TYPE_JPG = MediaType.parse("image/jpg");

        OkHttpClient client = new OkHttpClient();
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);

        for (Object file : mFiles.entrySet()) {
            Map.Entry mapEntry = (Map.Entry) file;
            String keyValue = (String) mapEntry.getKey();
            DataPart value = (DataPart) mapEntry.getValue();
            Log.v("test", keyValue + " " + value.getFileName());
            builder.addFormDataPart(keyValue, value.getFileName(), RequestBody.create(MEDIA_TYPE_JPG, value.getContent()));
        }

        for (Object file : mParams.entrySet()) {
            Map.Entry mapEntry = (Map.Entry) file;
            String keyValue = (String) mapEntry.getKey();
            String value = (String) mapEntry.getValue();
            Log.v("test", keyValue + " " + value);
            builder.addFormDataPart(keyValue, value);
        }

        builder.addFormDataPart("token", Config.getAuthToken(this));

        RequestBody requestBody = builder.build();

        final Request request = new Request.Builder()
                .addHeader("Content-Type", " application/x-www-form-urlencoded")
                .url(mode == Config.DRIVER_MODE ? Config.API_REG_DRIVER : Config.API_REG_LOADER)
                .post(requestBody)
                .build();

        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if(response.isSuccessful()) {
                        JSONObject resultResponse = new JSONObject(new String(response.body().string()));
                        Log.v("RegistrationResponse", resultResponse.toString());
                        if (resultResponse.getString("status").contains("success")) {
                            if (pd != null)
                                pd.dismiss();
                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                            startActivity(intent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /*VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, mode == Config.DRIVER_MODE ? Config.API_REG_DRIVER : Config.API_REG_LOADER, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    JSONObject resultResponse = new JSONObject(new String(response.data));
                    Log.v("RegistrationResponse", new String(response.data));
                    if(resultResponse.getString("status").contains("success")) {
                        if (pd != null)
                            pd.dismiss();
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    Log.e("RegistrationResponse", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("RegistrationResponse", error + "");
                error.printStackTrace();
                if (pd != null)
                    pd.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                mParams.put("token", Config.getAuthToken(getBaseContext()));
                return mParams;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                return mFiles;
            }
        };

        VolleySingleton.getInstance(getBaseContext()).addToRequestQueue(multipartRequest);*/
    }

    public void onChoice(View v) {
        Fragment personal;

        if(v.getId() == R.id.driver_mode) {
            setTitle("Информация о себе");
            personal = PersonalRegistrationFragment.newInstance(Config.DRIVER_MODE);
            mode = Config.DRIVER_MODE;
        }
        else {
            setTitle("Информация о себе");
            personal = PersonalRegistrationFragment.newInstance(Config.LOADER_MODE);
            mode = Config.LOADER_MODE;
        }

        showFragment(personal);
    }

    private void onExit() {
        Config.setAuthToken(null, this);
        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivity(intent);
        finish();
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);
        ft.add(R.id.fragmentContainer, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
