package com.dntm.avtoletperformer;


import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class PlaceAdapter extends ArrayAdapter<Place> {

    public PlaceAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public PlaceAdapter(Context context, int resource, List<Place> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.place_list_item, null);
        }

        Place p = getItem(position);

        if (p != null) {
            TextView placePrimaryText = (TextView) v.findViewById(R.id.placePrimaryText);
            TextView placeSecondaryText = (TextView) v.findViewById(R.id.placeSecondaryText);

            placePrimaryText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            placeSecondaryText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));

            if (placePrimaryText != null)
                placePrimaryText.setText(p.getPrimaryText());

            if (placeSecondaryText != null)
                placeSecondaryText.setText(p.getSecondaryText());
        }

        return v;
    }
}
