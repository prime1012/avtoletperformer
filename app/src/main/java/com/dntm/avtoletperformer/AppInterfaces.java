package com.dntm.avtoletperformer;

import com.dntm.avtoletperformer.VolleyMultipartRequest.DataPart;

import java.util.Map;

public class AppInterfaces {
    public interface onRegistration {
        public void onPersonalCompleted(Map<String, String> params, Map<String, DataPart> files);
        public void onAutoCompleted(Map<String, String> params, Map<String, DataPart> files);
        public void onDocumentsCompleted(Map<String, DataPart> files);
    }
    public interface onPayment {
        public void onCreatePayment(int amount);
        public void onPaymentFinished();
    }
    public interface onProfileChange {
        public void onBalanceChange();
    }
}
