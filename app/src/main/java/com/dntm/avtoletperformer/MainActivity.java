package com.dntm.avtoletperformer;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.Crashlytics;
import com.redmadrobot.inputmask.MaskedTextChangedListener;
import io.fabric.sdk.android.Fabric;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        if(Config.getAuthToken(this).length() == 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
            finish();
        } else {
            final String requestUrl = Config.API_GET_INFO_PERFORMER + "?token=" + Config.getAuthToken(this);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    requestUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("GetInfoResponse", response.toString());
                            try {
                                Intent intent;
                                getSharedPreferences("infoPerformer", 0).edit().putString("json", response.toString()).commit();

                                String firebase_token = Config.getFirebaseToken(getBaseContext());
                                if(firebase_token.length() != 0)
                                    sendRegistrationToServer(firebase_token);

                                if(response.getBoolean("need_registration"))
                                    intent = new Intent(getBaseContext(), RegistrationActivity.class);
                                else
                                    intent = new Intent(getBaseContext(), NavigationActivity.class);

                                if(!response.getBoolean("hasActiveOrder") && getSharedPreferences("activeOrder", 0).getString("fare", null) == null) {
                                    getSharedPreferences("activeOrder", 0).edit().clear().commit();
                                    getSharedPreferences("messagesClient", 0).edit().clear().commit();
                                }

                                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                startActivity(intent);
                                finish();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.getClass() == AuthFailureError.class) {
                                Config.setAuthToken(null, MainActivity.this);
                                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                Toast.makeText(MainActivity.this, "Не удалось выполнить авторизацию!", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(MainActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                            Log.d("GetInfoResponseError", error + "");
                        }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
            requestQueue.add(jsonObjectRequest);
        }
    }

    private void sendRegistrationToServer(final String token) {
        final String requestUrl = Config.API_SEND_FIREBASE_TOKEN + "?token=" + Config.getAuthToken(this) + "&firebase_token=" + token;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("FirebaseSendToServer", response.toString());
                        try {
                            Log.v("FirebaseSendToServer", token);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                        Log.d("FirebaseSendToServer", error + "");
                    }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        requestQueue.add(jsonObjectRequest);
    }

    private static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    private static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
