package com.dntm.avtoletperformer;


public class Performer {
    private Integer profileId;
    private Integer profileType;
    private String profileName;
    private String profilePhone;
    private Double profileRating;
    private Integer profileStatus;

    public Performer(Integer profileId, Integer profileType, String profileName, String profilePhone, Double profileRating, Integer profileStatus) {
        this.profileId = profileId;
        this.profileType = profileType;
        this.profileName = profileName;
        this.profilePhone = profilePhone;
        this.profileRating = profileRating;
        this.profileStatus = profileStatus;
    }

    public Integer getProfileId() {
        return this.profileId;
    }
    public Integer getProfileType() {
        return this.profileType;
    }
    public String getProfileName() {
        return this.profileName;
    }
    public String getProfilePhone() {
        return this.profilePhone;
    }
    public Double getProfileRating() {
        return this.profileRating;
    }
    public Integer getProfileStatus() {
        return this.profileStatus;
    }
}
