package com.dntm.avtoletperformer;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatCheckBox;
import android.widget.CheckBox;

public class CustomRadioButton extends AppCompatRadioButton {

    public CustomRadioButton(Context context) {
        super(context);
        init(context);
    }

    public CustomRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setTypeface(ResourcesCompat.getFont(context, R.font.cuprum_regular));
        setTextColor(getResources().getColor(R.color.colorPressed));
    }

    @Override
    public void setChecked(boolean t) {
        if(t) {
            this.setTextColor(getResources().getColor(R.color.colorWhite));
            this.setBackgroundResource(R.drawable.selected_checkbox);
        }
        else {
            this.setTextColor(getResources().getColor(R.color.colorPressed));
            this.setBackgroundResource(R.drawable.deselected_checkbox);
        }
        super.setChecked(t);
    }
}