package com.dntm.avtoletperformer;

import android.Manifest;
import android.app.Activity;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dntm.avtoletperformer.AppInterfaces.onRegistration;
import com.dntm.avtoletperformer.VolleyMultipartRequest.AppHelper;
import com.dntm.avtoletperformer.VolleyMultipartRequest.DataPart;
import com.mvc.imagepicker.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class DocumentsRegistrationFragment extends Fragment {
    Activity mActivity;
    int mCurrentPhotoPickerId;
    ImageView passportPhotoView, passportVisaPhotoView, driverLicensePhotoView, STSPhotoView;
    byte[] passportPhoto, passportVisaPhoto, driverLicensePhoto, STSPhoto;
    onRegistration registration;

    public static DocumentsRegistrationFragment newInstance(int mode) {
        DocumentsRegistrationFragment fragment = new DocumentsRegistrationFragment();

        Bundle args = new Bundle();
        args.putInt("mode", mode);
        fragment.setArguments(args);

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(getArguments().getInt("mode") == 0 ? R.layout.registration_documents_driver : R.layout.registration_documents_loader, null);

        passportPhotoView = (ImageView) v.findViewById(R.id.photoPassportReg);
        passportVisaPhotoView = (ImageView) v.findViewById(R.id.photoPassportVisaReg);
        driverLicensePhotoView = (ImageView) v.findViewById(R.id.photoDriverLicenseReg);
        STSPhotoView = (ImageView) v.findViewById(R.id.photoSTSReg);

        View.OnClickListener photoPicker = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPhotoPickerId = view.getId();

                if (ActivityCompat.checkSelfPermission(mActivity.getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED)
                    startPhotoPickerDialog();
                else
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
            }
        };

        ((LinearLayout) v.findViewById(R.id.photoPassportPicker)).setOnClickListener(photoPicker);

        if(getArguments().getInt("mode") == 1)
            ((LinearLayout) v.findViewById(R.id.photoPassportVisaPicker)).setOnClickListener(photoPicker);
        else {
            ((LinearLayout) v.findViewById(R.id.photoDriverLicensePicker)).setOnClickListener(photoPicker);
            ((LinearLayout) v.findViewById(R.id.photoSTSPicker)).setOnClickListener(photoPicker);
        }

        ((Button) v.findViewById(R.id.readyReg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getArguments().getInt("mode") == 1 && (passportPhoto == null || passportVisaPhoto == null)) {
                    Toast.makeText(mActivity, "Добавьте все необходимые фотографии", Toast.LENGTH_LONG).show();
                    return;
                } else if(getArguments().getInt("mode") == 0 && (passportPhoto == null || driverLicensePhoto == null || STSPhoto == null)) {
                    Toast.makeText(mActivity, "Добавьте все необходимые фотографии", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Map<String, DataPart> files = new HashMap<>();

                    files.put("photo_passport", new DataPart("photo_passport.jpg", passportPhoto, "image/jpeg"));
                    if(getArguments().getInt("mode") == 0) {
                        files.put("photo_license", new DataPart("photo_license.jpg", driverLicensePhoto, "image/jpeg"));
                        files.put("photo_sts", new DataPart("photo_sts.jpg", STSPhoto, "image/jpeg"));
                    } else
                        files.put("photo_passport_visa", new DataPart("photo_passport_visa.jpg", passportVisaPhoto, "image/jpeg"));

                    registration.onDocumentsCompleted(files);
                }
            }
        });

        return v;
    }

    private boolean validateImage(int[] images){
        for(int i = 0; i < images.length; i++){
            if(images[i] == 0){
                Toast.makeText(mActivity, "Добавьте все необходимые фотографии", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
        
        try {
            registration = (onRegistration) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onRegistration");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(mActivity, requestCode, resultCode, data);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);

            int dimension = getSquareCropDimensionForBitmap(bitmap);
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension);

            RoundedBitmapDrawable roundedBitmap = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
            roundedBitmap.setCircular(true);

            switch (mCurrentPhotoPickerId) {
                case R.id.photoPassportPicker:
                    passportPhoto = bos.toByteArray();
                    passportPhotoView.setImageDrawable(roundedBitmap);
                    passportPhotoView.setAlpha((float) 1);
                    ((TextView) getView().findViewById(R.id.photoPassportLabel)).setText("Изменить фотографию");
                    break;
                case R.id.photoPassportVisaPicker:
                    passportVisaPhoto = bos.toByteArray();
                    passportVisaPhotoView.setImageDrawable(roundedBitmap);
                    passportVisaPhotoView.setAlpha((float) 1);
                    ((TextView) getView().findViewById(R.id.photoPassportVisaLabel)).setText("Изменить фотографию");
                    break;
                case R.id.photoDriverLicensePicker:
                    driverLicensePhoto = bos.toByteArray();
                    driverLicensePhotoView.setImageDrawable(roundedBitmap);
                    driverLicensePhotoView.setAlpha((float) 1);
                    ((TextView) getView().findViewById(R.id.photoDriverLicenseLabel)).setText("Изменить фотографию");
                    break;
                case R.id.photoSTSPicker:
                    STSPhoto = bos.toByteArray();
                    STSPhotoView.setImageDrawable(roundedBitmap);
                    STSPhotoView.setAlpha((float) 1);
                    ((TextView) getView().findViewById(R.id.photoSTSLabel)).setText("Изменить фотографию");
                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startPhotoPickerDialog();
            }
        }
    }

    private int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public void startPhotoPickerDialog() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        ImagePicker.pickImage(DocumentsRegistrationFragment.this, "Выберите фотографию:");
    }
}
