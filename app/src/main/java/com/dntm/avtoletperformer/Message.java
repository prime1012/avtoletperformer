package com.dntm.avtoletperformer;

public class Message {
    private Integer userId;
    private Long timestamp;
    private String text;

    Message(Integer userId, String text, Long timestamp) {
        this.userId = userId;
        this.text = text;
        this.timestamp = timestamp;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
