package com.dntm.avtoletperformer.Notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationListener;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.dntm.avtoletperformer.CancelingConfirmationOrder;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.Message;
import com.dntm.avtoletperformer.NavigationActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null || Config.getAuthToken(getApplicationContext()).length() == 0)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.isNull("message") ? "" : data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            boolean repeatSound = false;
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            JSONObject payload = data.getJSONObject("payload");

            Log.e(TAG, "title: " + title);
            Log.e(TAG, "message: " + message);
            Log.e(TAG, "isBackground: " + isBackground);
            Log.e(TAG, "payload: " + payload.toString());
            Log.e(TAG, "imageUrl: " + imageUrl);
            Log.e(TAG, "timestamp: " + timestamp);

            Intent pushNotification = new Intent(payload.getString("action"));
            pushNotification.putExtra("payload", payload.toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            //Получение нового сообщение
            if(payload.getString("action").contains(Config.PUSH_NEW_MESSAGE)) {
                SharedPreferences messagesClient = getSharedPreferences("messagesClient", MODE_PRIVATE);
                JSONObject newMessage = new JSONObject();
                JSONArray messagesJsonArray = new JSONArray();

                if(messagesClient.getString("messages", null) != null)
                    messagesJsonArray = new JSONArray(messagesClient.getString("messages", null));

                newMessage.put("userId", payload.getInt("userId"));
                newMessage.put("text", payload.getString("text"));
                newMessage.put("timestamp", payload.getLong("timestamp"));
                messagesJsonArray.put(newMessage);
                messagesClient.edit().putString("messages", messagesJsonArray.toString()).apply();
            }

            if(payload.getString("action").contains(Config.PUSH_ORDER_CANCELED)) {
                getSharedPreferences("activeOrder", 0).edit().clear().apply();
                getSharedPreferences("messagesClient", 0).edit().clear().apply();
                Config.setInfoPerformer(this,"status", 1);
            }

            if(payload.getString("action").contains(Config.PUSH_PERFORMERS_STATUS_CHANGED))
                getSharedPreferences("activeOrder", 0).edit().putString("performers", payload.getJSONArray("performers").toString()).apply();

            if(payload.getString("action").contains(Config.PUSH_PERFORMER_LOCK_STATUS))
                Config.setInfoPerformer(this,"status", payload.getInt("status"));

            if(payload.getString("action").contains(Config.PUSH_PERFORMER_ACCESS_STATUS))
                Config.setInfoPerformer(this,"validate", payload.getBoolean("validate"));

            if(payload.getString("action").contains(Config.PUSH_NEW_ORDER))
                repeatSound = true;

            if(!isBackground) {
                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, repeatSound);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, imageUrl, repeatSound);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, boolean repeatSound) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(title, message, timeStamp, repeatSound);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, String imageUrl, boolean repeatSound) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(title, message, timeStamp, imageUrl, repeatSound);
    }
}