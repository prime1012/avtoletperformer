package com.dntm.avtoletperformer.Notification;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Config;
import com.dntm.avtoletperformer.NavigationActivity;
import com.dntm.avtoletperformer.R;
import com.dntm.avtoletperformer.RegistrationActivity;
import com.dntm.avtoletperformer.SplashScreenActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        Config.setFirebaseToken(this, refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // sending token to log
        Log.v("Firebase", refreshedToken);
    }

    private void sendRegistrationToServer(final String token) {
        if(Config.getAuthToken(this).length() > 0) {
            final String requestUrl = Config.API_SEND_FIREBASE_TOKEN + "?token=" + Config.getAuthToken(this) + "&firebase_token=" + token;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    requestUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("FirebaseSendToServer", response.toString());
                            try {
                                Log.v(TAG, "FirebaseSendToServer " + token);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplication(), "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                    Log.d("FirebaseSendToServer", error + "");
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
            requestQueue.add(jsonObjectRequest);
        }
    }
}
