package com.dntm.avtoletperformer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FinishOrderActivity extends AppCompatActivity {
    ProgressDialog pd;
    SharedPreferences activeOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_order);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Завершение заказа");
        toolbar.setNavigationIcon(R.drawable.ic_close);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        activeOrder = getSharedPreferences("activeOrder", 0);

        try {
            JSONArray fare = new JSONArray(activeOrder.getString("fare", null));
            ((TextView) findViewById(R.id.totalPrice)).setText(String.valueOf(activeOrder.getInt("totalPrice", 0)) + " ₽");

            FareAdapter fareAdapter = new FareAdapter(FinishOrderActivity.this, R.layout.fare_list_item);
            List<Fare> fareList = new ArrayList<Fare>();

            for (int i = 0; i < fare.length(); i++) {
                JSONObject f = fare.getJSONObject(i);
                fareList.add(new Fare(
                        f.getString("name"),
                        f.getInt("minPrice"),
                        f.getInt("minTime"),
                        f.getInt("overTimePrice"),
                        f.getInt("overTime"),
                        f.getInt("intercityPrice"),
                        f.getInt("intercityDistance"),
                        f.getInt("highDemandAreaPrice"),
                        f.getInt("highDemandTimePrice")
                ));
            }

            fareAdapter.addAll(fareList);
            fareAdapter.notifyDataSetChanged();

            LinearLayout layout = (LinearLayout) findViewById(R.id.detailFare);

            final int adapterCount = fareAdapter.getCount();

            for (int i = 0; i < adapterCount; i++) {
                View item = fareAdapter.getView(i, null, null);
                if(i != adapterCount - 1) {
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    lp.setMargins(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));
                    item.setLayoutParams(lp);
                }
                layout.addView(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBar.getProgress() != 100) {
                    seekBar.setProgress(0);
                } else {
                    final Integer orderId = activeOrder.getInt("orderId", -1);

                    pd = new ProgressDialog(FinishOrderActivity.this);
                    pd.setMessage("Пожалуйста, подождите");
                    pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pd.setIndeterminate(true);
                    pd.show();

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                            Config.API_FINISH_ORDER
                                    + "?orderId=" + orderId
                                    + "&rating=" + ((RatingBar) findViewById(R.id.ratingBar)).getRating()
                                    + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    if (pd != null)
                                        pd.dismiss();
                                    try {
                                        Log.d("FinishOrderResponse", response.toString());

                                        //Отправка события для аналитики доходов
                                        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(FinishOrderActivity.this);
                                        Bundle bundle = new Bundle();
                                        bundle.putString(FirebaseAnalytics.Param.CURRENCY, "RUB");
                                        bundle.putDouble(FirebaseAnalytics.Param.VALUE, response.getInt("commission"));
                                        bundle.putDouble(FirebaseAnalytics.Param.TRANSACTION_ID, orderId);
                                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);

                                        Config.setInfoPerformer(FinishOrderActivity.this,"balance", response.getInt("balance"));
                                        Config.setInfoPerformer(FinishOrderActivity.this,"status", 1);
                                        getSharedPreferences("activeOrder", 0).edit().clear().apply();
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (pd != null)
                                pd.dismiss();
                            Toast.makeText(FinishOrderActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                            Log.d("FinishOrderResponse", error + "");
                        }
                    });
                    RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                            0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjectRequest);

                    seekBar.setProgress(0);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
