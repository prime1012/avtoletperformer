package com.dntm.avtoletperformer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.Locale;

public class VerifyLoginActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_login);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

        final Intent intent = getIntent();
        final EditText smsCode = (EditText) findViewById(R.id.SMSCodeLogin);
        final TextView countDownTimer = (TextView) findViewById(R.id.countDownTimerLogin);
        final Button resendSmsCode = (Button) findViewById(R.id.resendSmsCodeLogin);
        final Button changePhone = (Button) findViewById(R.id.changePhoneNumberLogin);
        final Context context = this;

        //Отсчёт времени (120 секунд) до возможности повторно отправить код для получение токена
        final CountDownTimer timer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                countDownTimer.setText(String.format(Locale.ENGLISH,"%02d:%02d", ((millisUntilFinished / 1000) % 3600) / 60, (millisUntilFinished / 1000) % 60));
            }

            public void onFinish() {
                countDownTimer.setVisibility(View.GONE);
                resendSmsCode.setVisibility(View.VISIBLE);
            }
        }.start();
        timer.start();

        //Проверка кода на наличие 4-х цифр и отправка запроса на получение токена
        smsCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 4) {
                    try {
                        JSONObject params = new JSONObject();
                        params.put("phone_number", intent.getStringExtra("phoneNumber"));
                        params.put("code_confirmation", smsCode.getText());
                        String requestUrl = Config.API_REQUEST_AUTH;

                        pd = new ProgressDialog(context);
                        pd.setMessage("Пожалуйста, подождите");
                        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        pd.setIndeterminate(true);
                        pd.show();

                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                                requestUrl, params,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        Log.d("LoginVerifyResponse", response.toString());
                                        try {
                                            if (pd != null)
                                                pd.dismiss();
                                            Config.setAuthToken(response.getString("token"), context);
                                            if(!response.getBoolean("need_reg")) {
                                                Intent intent = new Intent(context, MainActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                                finish();
                                            } else {
                                                Intent intent = new Intent(context, RegistrationActivity.class);
                                                startActivity(intent);
                                                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                                finish();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (pd != null)
                                            pd.dismiss();
                                        AlertDialog.Builder builder = new AlertDialog.Builder(VerifyLoginActivity.this);
                                        builder.setTitle("Уведомление")
                                                .setMessage("Код указан неправильно. Попробуйте ещё раз.")
                                                .setCancelable(false)
                                                .setNegativeButton("ОК",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        });
                                        AlertDialog alert = builder.create();
                                        alert.show();
                                        smsCode.setText(null);
                                        Log.d("LoginVerifyResponse", error + "");
                                    }
                        });
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(jsonObjectRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //Повторная отправка кода для получение токена
        resendSmsCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String requestUrl = Config.API_REQUEST_SMS_CODE + "?phone_number=" + intent.getStringExtra("phoneNumber");
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                            requestUrl, null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("VerifyLoginResponse", response.toString());
                                    try {
                                        timer.start();
                                        countDownTimer.setVisibility(View.VISIBLE);
                                        resendSmsCode.setVisibility(View.GONE);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(context, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                                    Log.d("VerifyLoginResponse", error + "");
                                }
                    });
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(jsonObjectRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        changePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
