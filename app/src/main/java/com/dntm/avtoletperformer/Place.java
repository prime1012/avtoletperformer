package com.dntm.avtoletperformer;


public class Place {
    private String id;
    private String primaryText;
    private String secondaryText;

    Place(String id, String primaryText, String secondaryText) {
        this.id = id;
        this.primaryText = primaryText;
        this.secondaryText = secondaryText;
    }

    public String getId() {
        return id;
    }
    public String getPrimaryText() {
        return this.primaryText;
    }
    public String getSecondaryText() {
        return this.secondaryText;
    }
}
