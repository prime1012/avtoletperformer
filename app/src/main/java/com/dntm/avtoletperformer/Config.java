package com.dntm.avtoletperformer;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Config {
    public static int DRIVER_MODE = 0;
    public static int LOADER_MODE = 1;

    // Rest API Urls
    private static String API_SERVER_URL = "https://api.xn--24-6kci9br1ac8k.xn--p1ai/";
    public static String API_REQUEST_SMS_CODE = API_SERVER_URL + "api/requestSMSCodePerformer";
    public static String API_REQUEST_AUTH = API_SERVER_URL + "api/authenticatePerformers";
    public static String API_GET_INFO_PERFORMER = API_SERVER_URL + "api/performer/getInfo";
    public static String API_GET_TYPES_CARS = API_SERVER_URL + "api/getTypesCars";
    public static String API_REG_DRIVER = API_SERVER_URL + "api/performer/registrationDriver";
    public static String API_REG_LOADER = API_SERVER_URL + "api/performer/registrationLoader";
    public static String API_SEND_FIREBASE_TOKEN = API_SERVER_URL + "api/performer/setFirebaseToken";
    public static String PROFILE_PHOTO_DRIVER = API_SERVER_URL + "api/performer/photoDriver/";
    public static String PROFILE_PHOTO_LOADER = API_SERVER_URL + "api/performer/photoLoader/";
    public static String API_SEND_LOCATION = API_SERVER_URL + "api/performer/setLocation";
    public static String API_SET_STATUS_OFFLINE = API_SERVER_URL + "api/performer/setStatus?code=0";
    public static String API_SET_STATUS_ONLINE = API_SERVER_URL + "api/performer/setStatus?code=1";
    public static String API_SET_STATUS = API_SERVER_URL + "api/performer/setStatus";
    public static String API_CREATE_PAYMENT = API_SERVER_URL + "api/performer/createPayment";
    public static String API_PAYMENT_FINISHED = API_SERVER_URL + "api/paymentFinished";
    public static String API_GET_LAST_TRANSACTIONS = API_SERVER_URL + "api/performer/getLastTransactions";
    public static String API_GET_FINANCE = API_SERVER_URL + "api/performer/getFinance";
    public static String API_CONFIRM_ORDER = API_SERVER_URL + "api/performer/confirmOrder";
    public static String API_CANCEL_CONFIRMATION_ORDER = API_SERVER_URL + "api/performer/cancelOrderConfirmation";
    public static String API_FINISH_ORDER = API_SERVER_URL + "api/performer/finishOrder";
    public static String API_CANCEL_ORDER = API_SERVER_URL + "api/performer/cancelOrder";
    public static String API_GET_NEW_ORDERS = API_SERVER_URL + "api/performer/getNewOrders";
    public static String API_GET_LAST_ORDERS = API_SERVER_URL + "api/performer/getLastOrders";
    public static String API_GET_ORDER_INFO = API_SERVER_URL + "api/performer/getOrderInfo";
    public static String API_SEND_MESSAGE = API_SERVER_URL + "api/performer/sendMessage";
    public static String API_GET_SETTLEMENTS = API_SERVER_URL + "api/performer/getSettlements";

    public static String SUPPORT_PHONE_NUMBER = "tel:+73832123212";
    public static Double ACQUIRING_PERCENTAGE = 0.02;
    public static Integer ACQUIRING_MIN_AMOUNT_DRIVER = 500;
    public static Integer ACQUIRING_MIN_AMOUNT_LOADER = 300;

    // broadcast receiver intent filters
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String PUSH_NEW_ORDER = "newOrder";
    public static final String PUSH_ORDER_CONFIRMED = "orderConfirmed";
    public static final String PUSH_ORDER_CANCELED = "orderCanceled";
    public static final String PUSH_PERFORMERS_STATUS_CHANGED = "statusPerformerChanged";
    public static final String PUSH_PERFORMER_LOCK_STATUS = "performerLockStatus";
    public static final String PUSH_PERFORMER_ACCESS_STATUS = "performerAccessStatus";
    public static final String PUSH_NEW_MESSAGE = "newMessage";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    //Auth Token
    public static void setAuthToken(String token, Context context) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("token", token);
        editor.apply();
        return;
    }

    public static String getAuthToken(Context context) {
        SharedPreferences settings = PreferenceManager
                .getDefaultSharedPreferences(context);
        return settings.getString("token", "");
    }

    public static Object getInfoPerformer(Context context, String name) {
        SharedPreferences preferences = context.getSharedPreferences("infoPerformer", 0);
        Object object = "";

        try {
            JSONObject info = new JSONObject(preferences.getString("json", ""));
            object = info.get(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

    public static void setInfoPerformer(Context context, String name, Object value) {
        SharedPreferences preferences = context.getSharedPreferences("infoPerformer", 0);
        Object object = "";

        try {
            JSONObject info = new JSONObject(preferences.getString("json", ""));
            info.put(name, value);

            preferences.edit().putString("json", info.toString()).commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Firebase token
    public static void setFirebaseToken(Context context, String token) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("firebase_id", token);
        editor.apply();
    }
    public static String getFirebaseToken(Context context) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        return pref.getString("firebase_id", "");
    }
}
