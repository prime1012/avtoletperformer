package com.dntm.avtoletperformer;


import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.NOTIFICATION_SERVICE;

public class CancelingConfirmationOrder extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Отмена повторяющегося сигнала о новом заказе
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        //Отказ от нового заказа по истечении 40 сек
        try {
            final JSONObject routeData = new JSONObject(context.getSharedPreferences("newOrder", 0).getString("routeData", null));
            context.getSharedPreferences("newOrder", 0).edit().clear().apply();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                    Config.API_CANCEL_CONFIRMATION_ORDER
                            + "?orderId=" + routeData.getInt("orderId")
                            + "&token=" + Config.getAuthToken(context), null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("CancelOrderResponse", response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("CancelOrderResponse", error + "");
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
