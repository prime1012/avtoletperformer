package com.dntm.avtoletperformer;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

public class DotAutoFillTextView extends AppCompatTextView {

    private int availableWidthForDots;
    private int widthOfSpace;
    private int widthOfDotWithSpace;

    public DotAutoFillTextView(Context context) {
        super(context);
    }

    public DotAutoFillTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DotAutoFillTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int width = getWidth() - getPaddingLeft() - getPaddingRight();
        int lastLineWidth = (int) getLayout().getLineWidth(getLineCount() - 1);
        availableWidthForDots = width - lastLineWidth;
        int widthOfOneDot = getWidthOfOneDot();
        int widthOfTwoDotsWithSpace = getWidthOfTwoDotsWithSpace();
        widthOfSpace = widthOfTwoDotsWithSpace - (widthOfOneDot * 2);
        widthOfDotWithSpace = widthOfSpace + widthOfOneDot;
        int requiredDots = getRequiredDotsNumber();
        if (requiredDots != 0) {
            int spaces = getRequiredSpacesNumber(requiredDots);
            StringBuilder result = new StringBuilder();
            String text = getText().toString();
            result.append(text.substring(0, text.lastIndexOf(' ')));
            setText(result.toString());
            StringBuilder dots = new StringBuilder();
            for (int i = 0; i < requiredDots; ++i) {
                dots.append(" .");
            }
            for (int i = 0; i < spaces; ++i) {
                dots.append(" ");
            }
            result.append(dots.toString());
            result.append(text.substring(text.lastIndexOf(' ') + 1));
            setText(result.toString());
        }
        super.onLayout(changed, left, top, right, bottom);
    }

    private int getRequiredSpacesNumber(int requiredDots) {
        float remain = (1f * availableWidthForDots) % (1f * widthOfDotWithSpace);
        return (int) ((remain / widthOfSpace) + 0.5f);
    }

    private int getRequiredDotsNumber() {
        if (getLayout() == null) {
            return 1;
        }
        int numberOfDots = availableWidthForDots / widthOfDotWithSpace;
        return numberOfDots;
    }

    private int getWidthOfTwoDotsWithSpace() {
        return getStringWidth(". .");
    }

    private int getWidthOfOneDot() {
        return getStringWidth(".");
    }

    private int getStringWidth(String text) {
        Rect dotBounds = new Rect();
        getPaint().getTextBounds(text, 0, text.length(), dotBounds);
        return dotBounds.width();
    }
}
