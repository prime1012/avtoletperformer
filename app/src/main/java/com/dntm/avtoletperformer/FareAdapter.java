package com.dntm.avtoletperformer;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class FareAdapter extends ArrayAdapter<Fare> {
    private Context mContext;

    public FareAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mContext = context;
    }

    public FareAdapter(Context context, int resource, List<Fare> items) {
        super(context, resource, items);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.fare_list_item, null);
        }

        Fare fare = getItem(position);

        ((TextView) v.findViewById(R.id.name)).setText(fare.getName());
        ((TextView) v.findViewById(R.id.minPrice)).setText(fare.getMinPrice().toString() + " ₽");

        if(fare.getOverTimePrice() > 0) {
            Long overTimeHour = TimeUnit.SECONDS.toHours(fare.getOverTime());
            Long overTimeMinute = TimeUnit.SECONDS.toMinutes(fare.getOverTime() - (overTimeHour * 3600));

            String overTime = overTimeHour > 0 ?
                    String.format(Locale.US,"+ %s %s",getHourTimeString(overTimeHour), getMinuteTimeString(overTimeMinute)) :
                    String.format(Locale.US,"+ %s", getMinuteTimeString(overTimeMinute));

            ((LinearLayout) v.findViewById(R.id.overTimeLabel)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.overTime)).setText(overTime);
            ((TextView) v.findViewById(R.id.overTimePrice)).setText(fare.getOverTimePrice() + " ₽");
        }

        if(fare.getIntercityPrice() > 0) {
            ((LinearLayout) v.findViewById(R.id.intercityLabel)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.intercityDistance)).setText(String.format(Locale.US,"+ %.0f", Math.floor(fare.getIntercityDistance())) + " км (межгород)");
            ((TextView) v.findViewById(R.id.intercityPrice)).setText(fare.getIntercityPrice().toString() + " ₽");
        }

        if(fare.getHighDemandAreaPrice() > 0) {
            ((LinearLayout) v.findViewById(R.id.highDemandAreaLabel)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.highDemandAreaPrice)).setText(fare.getHighDemandAreaPrice().toString() + " ₽");
        }

        if(fare.getHighDemandTimePrice() > 0) {
            ((LinearLayout) v.findViewById(R.id.highDemandTime)).setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.highDemandTimePrice)).setText(fare.getHighDemandTimePrice().toString() + " ₽");
        }

        return v;
    }

    private String getHourTimeString(long hour) {
        if(hour == 1)
            return "1 час";
        else if(hour > 1 && hour < 5)
            return hour + " часа";
        else if(hour >= 5)
            return hour + " часов";
        return "";
    }
    private String getMinuteTimeString(long minute) {
        if(minute == 1)
            return "1 минута";
        else if(minute > 1 && minute < 5)
            return minute + " минуты";
        else if(minute >= 5)
            return minute + " минут";
        return "";
    }
}
