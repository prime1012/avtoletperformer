package com.dntm.avtoletperformer;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dntm.avtoletperformer.VolleyMultipartRequest.AppHelper;
import com.dntm.avtoletperformer.VolleyMultipartRequest.DataPart;
import com.mvc.imagepicker.ImagePicker;
import com.mvc.imagepicker.ImageRotator;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import android.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class PersonalRegistrationFragment extends Fragment {
    Activity mActivity;
    ImageView profilePhotoView;
    Button settlement;
    byte[] profilePhoto;
    AppInterfaces.onRegistration registration;

    public static PersonalRegistrationFragment newInstance(int mode) {
        PersonalRegistrationFragment fragment = new PersonalRegistrationFragment();

        Bundle args = new Bundle();
        args.putInt("mode", mode);
        fragment.setArguments(args);

        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(getArguments().getInt("mode") == 0 ? R.layout.registration_personal_driver : R.layout.registration_personal_loader, null);

        profilePhotoView = (ImageView) v.findViewById(R.id.profilePhotoReg);

        final EditText dateOfBirth = (EditText) v.findViewById(R.id.dateOfBirthReg);
        final EditText name = (EditText) v.findViewById(R.id.nameReg);
        final EditText surname = (EditText) v.findViewById(R.id.surnameReg);
        final EditText middleName = (EditText) v.findViewById(R.id.middleNameReg);
        final EditText SSN = (EditText) v.findViewById(R.id.SSNReg);
        final RadioGroup entrepreneur = (RadioGroup) v.findViewById(R.id.entrepreneurReg);
        final RadioGroup partTimeOfLoader = (RadioGroup) v.findViewById(R.id.partTimeOfLoaderReg);
        final RadioGroup forwader = (RadioGroup) v.findViewById(R.id.forwaderReg);
        settlement = (Button) v.findViewById(R.id.settlementReg);

        final TextView.OnEditorActionListener clearFocus = new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    v.clearFocus();
                    InputMethodManager inputManager = (InputMethodManager)
                            mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.toggleSoftInput(0, 0);
                }
                return false;
            }
        };

        if(getArguments().getInt("mode") == 1)
            dateOfBirth.setOnEditorActionListener(clearFocus);
        else
            SSN.setOnEditorActionListener(clearFocus);

        name.setOnEditorActionListener(clearFocus);
        surname.setOnEditorActionListener(clearFocus);
        middleName.setOnEditorActionListener(clearFocus);

        if(getArguments().getInt("mode") == 1) {
            final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                    "[00].[00].[0000]",
                    false,
                    dateOfBirth,
                    null,
                    new MaskedTextChangedListener.ValueListener() {
                        @Override
                        public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        }
                    }
            );

            dateOfBirth.addTextChangedListener(listener);
            dateOfBirth.setOnFocusChangeListener(listener);
            dateOfBirth.setHint(listener.placeholder());
        } else {
            entrepreneur.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if(i == R.id.entrepreneurAgreeReg) {
                        ((TextView) getView().findViewById(R.id.SSNLabel)).setVisibility(View.VISIBLE);
                        ((EditText) getView().findViewById(R.id.SSNReg)).setVisibility(View.VISIBLE);
                    } else {
                        ((TextView) getView().findViewById(R.id.SSNLabel)).setVisibility(View.GONE);
                        ((EditText) getView().findViewById(R.id.SSNReg)).setVisibility(View.GONE);
                    }
                }
            });
        }

        ((LinearLayout) v.findViewById(R.id.photoPickerButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Требования к фотографии")
                        .setMessage("На фотографии должно быть отчетливо видно лицо исполнителя (овал лица занимает большую часть фотографии). На фотографии не должно быть изображено несколько человек, автомобиль водителя или иных посторонних предметов.")
                        .setCancelable(false)
                        .setPositiveButton("Продолжить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (ActivityCompat.checkSelfPermission(mActivity.getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                == PackageManager.PERMISSION_GRANTED)
                                            startPhotoPickerDialog();
                                        else
                                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        ((Button) v.findViewById(R.id.readyReg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText[] loader = {surname, name, middleName, dateOfBirth};
                EditText[] driver = {surname, name, middleName};

                if(getArguments().getInt("mode") == 0) {
                    if (entrepreneur.getCheckedRadioButtonId() == R.id.entrepreneurAgreeReg && !validateText(new EditText[]{SSN}))
                        return;
                }

                if(profilePhoto == null) {
                    Toast.makeText(mActivity, "Добавьте все необходимые фотографии", Toast.LENGTH_LONG).show();
                    return;
                }

                if(settlement.getTag() == null) {
                    Toast.makeText(mActivity, "Необходимо указать город", Toast.LENGTH_LONG).show();
                    return;
                }

                if(validateText(getArguments().getInt("mode") == 0 ? driver : loader)) {
                    Map<String, String> params = new HashMap<>();
                    Map<String, DataPart> files = new HashMap<>();

                    params.put("name", name.getText().toString());
                    params.put("surname", surname.getText().toString());
                    params.put("middleName", middleName.getText().toString());
                    params.put("settlementId", settlement.getTag().toString());
                    if(getArguments().getInt("mode") == 1) {
                        params.put("dateOfBirth", dateOfBirth.getText().toString());
                        files.put("photo_loader", new DataPart("photo_loader.jpg", profilePhoto, "image/jpeg"));
                    }
                    else {
                        params.put("SSN", SSN.getText().toString());
                        params.put("partTimeOfLoader", partTimeOfLoader.getCheckedRadioButtonId() == R.id.loaderAgreeReg ? "true" : "false");
                        params.put("forwader", forwader.getCheckedRadioButtonId() == R.id.forwaderAgreeReg ? "true" : "false");
                        params.put("entrepreneur", entrepreneur.getCheckedRadioButtonId() == R.id.entrepreneurAgreeReg ? "true" : "false");
                        files.put("photo_driver", new DataPart("photo_driver.jpg", profilePhoto, "image/jpeg"));
                    }

                    registration.onPersonalCompleted(params, files);
                }
            }
        });

        settlement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, CitySelectorActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
        
        try {
            registration = (AppInterfaces.onRegistration) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onRegistration");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            if(requestCode == 1) {
                settlement.setText(data.getStringExtra("name"));
                settlement.setTag(data.getStringExtra("id"));
            } else {
                Bitmap bitmap = ImagePicker.getImageFromResult(mActivity, requestCode, resultCode, data);

                if (bitmap == null) {
                    Toast.makeText(getActivity(), "Произошла ошибка. Попробуйте ещё раз.", Toast.LENGTH_LONG).show();
                    return;
                }

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                profilePhoto = bos.toByteArray();

                //int dimension = getSquareCropDimensionForBitmap(bitmap);
                bitmap = ThumbnailUtils.extractThumbnail(bitmap, 120, 120);

                RoundedBitmapDrawable roundedBitmap = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                roundedBitmap.setCircular(true);
                profilePhotoView.setImageDrawable(roundedBitmap);
                profilePhotoView.setAlpha((float) 1);
                ((TextView) getView().findViewById(R.id.photoPickerText)).setText("Изменить фотографию");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startPhotoPickerDialog();
            }
        }
    }

    private boolean validateText(EditText[] fields){
        for(int i = 0; i < fields.length; i++){
            EditText currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                currentField.setBackgroundResource(R.drawable.rounded_text_error);
                currentField.requestFocus();
                return false;
            }
            currentField.setBackgroundResource(R.drawable.rounded_text);
        }
        return true;
    }

    private int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public void startPhotoPickerDialog() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        ImagePicker.pickImage(PersonalRegistrationFragment.this, "Выберите фотографию:");
    }
}
