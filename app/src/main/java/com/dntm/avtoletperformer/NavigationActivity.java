package com.dntm.avtoletperformer;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.Menu.BalanceFragment;
import com.dntm.avtoletperformer.Menu.FinanceFragment;
import com.dntm.avtoletperformer.Menu.LastOrdersFragment;
import com.dntm.avtoletperformer.Menu.NewOrdersFragment;
import com.dntm.avtoletperformer.Menu.SupportActivity;
import com.dntm.avtoletperformer.Menu.TopUpBalanceActivity;

import org.json.JSONObject;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AppInterfaces.onProfileChange {

    public DrawerLayout drawer;
    NavigationView navigationView;
    BroadcastReceiver orderCanceled, orderConfirmed;
    FloatingActionButton activeOrderLink;
    Switch statusSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        activeOrderLink = (FloatingActionButton) findViewById(R.id.activeOrderLink);

        statusSwitch = (Switch) findViewById(R.id.statusSwitch);
        statusSwitch.setTypeface(ResourcesCompat.getFont(this, R.font.cuprum_regular));

        setTitle("Заказы");
        showFragment(new NewOrdersFragment());

        final View drawerHeader = navigationView.getHeaderView(0);

        try {
            MenuItem nav_balance = navigationView.getMenu().findItem(R.id.nav_balance);
            nav_balance.setTitle("Баланс: " + Config.getInfoPerformer(this,"balance") + " \u20BD");

            ((TextView) drawerHeader.findViewById(R.id.profileName)).setText(Config.getInfoPerformer(this,"name").toString());
            ((TextView) drawerHeader.findViewById(R.id.profileRating)).setText(Config.getInfoPerformer(this,"rating").toString());

            /*try {
                String url = Config.PROFILE_PHOTO_DRIVER + Config.getInfoPerformer(this,"id") + "/photo_driver.jpg?token=" + Config.getAuthToken(this);
                if (Config.getInfoPerformer(this,"type").equals(1))
                    url = Config.PROFILE_PHOTO_LOADER + Config.getInfoPerformer(this,"id") + "/photo_loader.jpg?token=" + Config.getAuthToken(this);

                ImageRequest request = new ImageRequest(url,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {
                                int dimension = getSquareCropDimensionForBitmap(bitmap);
                                bitmap = ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension);

                                RoundedBitmapDrawable roundedBmp = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                                roundedBmp.setCircular(true);
                                ((ImageView) drawerHeader.findViewById(R.id.profilePhoto)).setImageDrawable(roundedBmp);
                            }
                        }, 0, 0, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                                Log.e("getProfilePhoto", error.toString());
                            }
                        });
                RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                requestQueue.add(request);
            } catch (Exception e) {
                Log.e("getProfilePhoto", e.toString());
            }*/

            if ((int) Config.getInfoPerformer(this,"status") > 0) {
                if (Integer.parseInt(Config.getInfoPerformer(this,"balance").toString()) <= 0) {
                    setStatusOffline();
                    statusSwitch.setChecked(false);
                    return;
                }
                shareLocation(true);
                statusSwitch.setChecked(true);
            }

            statusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (Integer.parseInt(Config.getInfoPerformer(NavigationActivity.this,"balance").toString()) <= 0) {
                        statusSwitch.setChecked(false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
                        builder.setTitle("Уведомление")
                                .setMessage("Для того чтобы начать принимать новые заказы, необходимо пополнить баланс внутри сервиса. С баланса будет списываться комиссия с каждого выполненного заказа.")
                                .setCancelable(false)
                                .setNegativeButton("Пополнить баланс",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                Intent intent = new Intent(NavigationActivity.this, TopUpBalanceActivity.class);
                                                startActivityForResult(intent, 0);
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                        return;
                    }

                    if (!Config.getInfoPerformer(NavigationActivity.this,"status").equals(1) && !Config.getInfoPerformer(NavigationActivity.this,"status").equals(0)) {
                        statusSwitch.setChecked(true);
                        Toast.makeText(NavigationActivity.this, "Переход в оффлайн режим невозможен", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (b) {
                        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                && (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                            setStatusOnline();
                        } else
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    } else
                        setStatusOffline();
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
            finish();
        }

        orderConfirmed = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                activeOrderLink.setVisibility(View.VISIBLE);
                activeOrderLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NavigationActivity.this, ActiveOrderActivity.class);
                        startActivity(intent);
                    }
                });
            }
        };

        orderCanceled = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                activeOrderLink.setVisibility(View.GONE);
                AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
                builder.setTitle("Уведомление")
                        .setMessage("К сожалению, клиент отменил заказ.")
                        .setCancelable(false)
                        .setNegativeButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(orderConfirmed, new IntentFilter(Config.PUSH_ORDER_CONFIRMED));
        LocalBroadcastManager.getInstance(this).registerReceiver(orderCanceled, new IntentFilter(Config.PUSH_ORDER_CANCELED));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(orderConfirmed);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(orderCanceled);
    }

    private int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public void setStatusOnline() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Config.API_SET_STATUS_ONLINE + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("SetStatusResponse", response.toString());
                        Config.setInfoPerformer(NavigationActivity.this,"status", 1);
                        shareLocation(true);
                        statusSwitch.setChecked(true);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NavigationActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("SetStatusResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        requestQueue.add(jsonObjectRequest);
    }

    private void setStatusOffline() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                Config.API_SET_STATUS_OFFLINE + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("SetStatusResponse", response.toString());
                        Config.setInfoPerformer(NavigationActivity.this,"status", 0);
                        shareLocation(false);
                        statusSwitch.setChecked(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NavigationActivity.this, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("SetStatusResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
        requestQueue.add(jsonObjectRequest);
    }

    public void shareLocation(boolean start) {
        Intent intent = new Intent(getApplicationContext(), LocationService.class);
        if(start) {
            //Get network access in Doze mode (Android 6.0 and higher)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Intent batterySettings = new Intent();
                String packageName = getPackageName();
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
                    batterySettings.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                    batterySettings.setData(Uri.parse("package:" + packageName));
                    startActivityForResult(batterySettings, 0);
                    return;
                }
            }

            //Проверка на включённые геоданные
            if(!isLocationEnabled(this)) {
                statusSwitch.setChecked(false);
                setStatusOffline();
                AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
                builder.setTitle("Уведомление")
                        .setMessage("Для корректной работы приложения необходимо включить геоданные")
                        .setCancelable(false)
                        .setPositiveButton("Включить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent turnOnLocation = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(turnOnLocation);
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
                return;
            }

            startService(intent);
        }
        else
            stopService(intent);
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
            boolean isIgnoringBatteryOptimizations = powerManager.isIgnoringBatteryOptimizations(getPackageName());
            if (!isIgnoringBatteryOptimizations) {
                statusSwitch.setChecked(false);
                setStatusOffline();
                AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this);
                builder.setTitle("Уведомление")
                        .setMessage("Для корректной работы приложения необходимо разрешить его работу в фоновом режиме")
                        .setCancelable(false)
                        .setPositiveButton("Разрешить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        statusSwitch.setChecked(true);
                                        setStatusOnline();
                                        shareLocation(true);
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            } else
                shareLocation(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Обновление баланса
        onBalanceChange();

        //Проверка статуса доступа к сервису
        if(!(boolean) Config.getInfoPerformer(this,"validate")) {
            Intent intent = new Intent(getBaseContext(), SplashScreenActivity.class);
            intent.putExtra("mode", 0);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            startActivity(intent);
            finish();
        } else if(Config.getInfoPerformer(this,"status").equals(-1)) {
            //Проверка статуса блокировки исполнителя
            Intent intent = new Intent(getBaseContext(), SplashScreenActivity.class);
            intent.putExtra("mode", -1);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            startActivity(intent);
            finish();
        }

        if(getSharedPreferences("activeOrder", 0).getInt("orderId", -1) != -1) {
            activeOrderLink.setVisibility(View.VISIBLE);
            activeOrderLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(getSharedPreferences("activeOrder", 0).getInt("status", -1) == 1) {
                        Intent intent = new Intent(NavigationActivity.this, FinishOrderActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(NavigationActivity.this, ActiveOrderActivity.class);
                        startActivity(intent);
                    }
                }
            });
        } else
            activeOrderLink.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle map view item clicks here.
        int id = item.getItemId();

        if(!item.isChecked()) {
            switch (id) {
                case R.id.nav_new_orders: {
                    setTitle("Заказы");
                    showFragment(new NewOrdersFragment());
                    break;
                }
                case R.id.nav_orders: {
                    setTitle("Последние заказы");
                    showFragment(new LastOrdersFragment());
                    break;
                }
                case R.id.nav_balance: {
                    setTitle("Баланс");
                    showFragment(new BalanceFragment());
                    break;
                }
                case R.id.nav_chart: {
                    setTitle("Статистика");
                    showFragment(new FinanceFragment());
                    break;
                }
                case R.id.nav_support: {
                    Intent intent = new Intent(this, SupportActivity.class);
                    this.startActivity(intent);
                    break;
                }
                /*case R.id.nav_exit: {
                    if(getSharedPreferences("activeOrder", 0).getInt("orderId", -1) != -1) {
                        Toast.makeText(NavigationActivity.this, "Переход в оффлайн режим невозможен", Toast.LENGTH_SHORT).show();
                        break;
                    }
                    Config.setAuthToken(null, this);
                    Intent intent = new Intent(this, LoginActivity.class);
                    this.startActivity(intent);
                    finish();
                    break;
                }*/
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setStatusOnline();
        }
    }

    public void onBalanceChange() {
        MenuItem nav_balance = navigationView.getMenu().findItem(R.id.nav_balance);
        nav_balance.setTitle("Баланс: " + Config.getInfoPerformer(this,"balance") + " \u20BD");
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.animator.slide_in, R.animator.slide_out);
        ft.add(R.id.content_navigation, fragment);
        ft.commit();
    }
}
