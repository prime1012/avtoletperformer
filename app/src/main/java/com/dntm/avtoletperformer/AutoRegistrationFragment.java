package com.dntm.avtoletperformer;

import android.Manifest;
import android.app.Activity;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.VolleyMultipartRequest.AppHelper;
import com.dntm.avtoletperformer.VolleyMultipartRequest.DataPart;
import com.mvc.imagepicker.ImagePicker;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class AutoRegistrationFragment extends Fragment {
    ProgressDialog pd;
    ImageView autoPhotoView;
    byte[] autoPhoto;
    AppInterfaces.onRegistration registration;
    Activity mActivity;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.registration_auto_driver, null);

        final String requestUrl = Config.API_GET_TYPES_CARS + "?token=" + Config.getAuthToken(mActivity.getBaseContext());
        pd = new ProgressDialog(mActivity);
        pd.setMessage("Пожалуйста, подождите");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                requestUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("GetTypesCarsResponse", response.toString());
                        try {
                            JSONArray categories = response.getJSONArray("categories");
                            JSONArray frames = response.getJSONArray("frames");

                            for (int i=0; i < categories.length(); i++) {
                                createList(categories.getJSONObject(i), R.id.categoriesAutoList);
                                ((RadioGroup) getView().findViewById(R.id.categoriesAutoList)).check(categories.getJSONObject(0).getInt("id"));
                            }

                            for (int i=0; i < frames.length(); i++) {
                                createList(frames.getJSONObject(i), R.id.framesAutoList);
                                ((RadioGroup) getView().findViewById(R.id.framesAutoList)).check(frames.getJSONObject(0).getInt("id"));
                            }
                            if (pd != null)
                                pd.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null)
                    pd.dismiss();
                Toast.makeText(mActivity, "Не удалось подключиться к серверу!", Toast.LENGTH_SHORT).show();
                Log.d("GetTypesCarsResponse", error + "");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(mActivity.getBaseContext());
        requestQueue.add(jsonObjectRequest);

        autoPhotoView = (ImageView) v.findViewById(R.id.autoPhotoReg);

        final EditText modelAuto = (EditText) v.findViewById(R.id.modelAutoReg);
        final EditText numberplateAuto = (EditText) v.findViewById(R.id.numberplateAutoReg);
        final EditText volumeAuto = (EditText) v.findViewById(R.id.volumeAutoReg);
        final EditText lengthAuto = (EditText) v.findViewById(R.id.lengthAutoReg);
        final EditText widthAuto = (EditText) v.findViewById(R.id.widthAutoReg);
        final EditText heightAuto = (EditText) v.findViewById(R.id.heightAutoReg);

        final TextView.OnEditorActionListener clearFocus = new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    v.clearFocus();
                    InputMethodManager inputManager = (InputMethodManager)
                            mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.toggleSoftInput(0, 0);
                }
                return false;
            }
        };

        modelAuto.setOnEditorActionListener(clearFocus);
        numberplateAuto.setOnEditorActionListener(clearFocus);
        volumeAuto.setOnEditorActionListener(clearFocus);
        lengthAuto.setOnEditorActionListener(clearFocus);
        widthAuto.setOnEditorActionListener(clearFocus);
        heightAuto.setOnEditorActionListener(clearFocus);

        final RadioGroup categories = (RadioGroup) v.findViewById(R.id.categoriesAutoList);
        final RadioGroup frames = (RadioGroup) v.findViewById(R.id.framesAutoList);

        final MaskedTextChangedListener numberPlateMask = new MaskedTextChangedListener(
                    "[A000AA][000]",
                    false,
                    numberplateAuto,
                    null,
                    new MaskedTextChangedListener.ValueListener() {
                        @Override
                        public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        }
                    }
        );

        numberplateAuto.addTextChangedListener(numberPlateMask);
        numberplateAuto.setOnFocusChangeListener(numberPlateMask);
        numberplateAuto.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        final MaskedTextChangedListener lengthAutoMask = new MaskedTextChangedListener(
                "[0].[00]",
                false,
                lengthAuto,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                    }
                }
        );

        final MaskedTextChangedListener widthAutoMask = new MaskedTextChangedListener(
                "[0].[00]",
                false,
                widthAuto,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                    }
                }
        );

        final MaskedTextChangedListener heightAutoMask = new MaskedTextChangedListener(
                "[0].[00]",
                false,
                heightAuto,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                    }
                }
        );

        lengthAuto.addTextChangedListener(lengthAutoMask);
        lengthAuto.setOnFocusChangeListener(lengthAutoMask);
        widthAuto.addTextChangedListener(widthAutoMask);
        widthAuto.setOnFocusChangeListener(widthAutoMask);
        heightAuto.addTextChangedListener(heightAutoMask);
        heightAuto.setOnFocusChangeListener(heightAutoMask);

        ((LinearLayout) v.findViewById(R.id.photoPickerButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(mActivity.getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED)
                    startPhotoPickerDialog();
                else
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},0);
            }
        });

        ((Button) v.findViewById(R.id.readyReg)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(autoPhoto == null) {
                    Toast.makeText(mActivity, "Добавьте все необходимые фотографии", Toast.LENGTH_LONG).show();
                    return;
                }

                if(validateText(new EditText[] {modelAuto, numberplateAuto, volumeAuto, lengthAuto, widthAuto, heightAuto})) {
                    Map<String, String> params = new HashMap<>();
                    Map<String, DataPart> files = new HashMap<>();

                    params.put("categoriesAuto", String.valueOf(categories.getCheckedRadioButtonId()));
                    params.put("framesAuto", String.valueOf(frames.getCheckedRadioButtonId()));
                    params.put("modelAuto", modelAuto.getText().toString());
                    params.put("numberplateAuto", numberplateAuto.getText().toString());
                    params.put("volumeAuto", volumeAuto.getText().toString());
                    params.put("lengthAuto", lengthAuto.getText().toString());
                    params.put("widthAuto", widthAuto.getText().toString());
                    params.put("heightAuto", heightAuto.getText().toString());
                    files.put("photo_auto", new DataPart("photo_auto.jpg", autoPhoto, "image/jpeg"));

                    registration.onAutoCompleted(params, files);
                }
            }
        });

        return v;
    }

    private void createList(JSONObject object, int radiogroup_id) {
        CustomRadioButton rb = new CustomRadioButton(mActivity.getBaseContext());
        try {
            RadioGroup.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(8, 8, 8, 0);

            rb.setId(object.getInt("id"));
            rb.setText(object.getString("name"));
            rb.setAllCaps(true);
            rb.setClickable(true);
            rb.setLayoutParams(layoutParams);
        } catch (JSONException e) { }
        ((RadioGroup) getView().findViewById(radiogroup_id)).addView(rb);
    }

    private boolean validateText(EditText[] fields){
        for(int i = 0; i < fields.length; i++){
            EditText currentField = fields[i];
            if(currentField.getText().toString().length() <= 0){
                currentField.setBackgroundResource(R.drawable.rounded_text_error);
                currentField.requestFocus();
                return false;
            }
            currentField.setBackgroundResource(R.drawable.rounded_text);
        }
        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;

        try {
            registration = (AppInterfaces.onRegistration) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onRegistration");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK) {
            Bitmap bitmap = ImagePicker.getImageFromResult(mActivity, requestCode, resultCode, data);

            if(bitmap == null) {
                Toast.makeText(getActivity(), "Произошла ошибка. Попробуйте ещё раз.", Toast.LENGTH_LONG).show();
                return;
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            autoPhoto = bos.toByteArray();

            //int dimension = getSquareCropDimensionForBitmap(bitmap);
            bitmap = ThumbnailUtils.extractThumbnail(bitmap, 120, 120);

            RoundedBitmapDrawable roundedBitmap = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
            roundedBitmap.setCircular(true);
            autoPhotoView.setImageDrawable(roundedBitmap);
            autoPhotoView.setAlpha((float) 1);
            ((TextView) getView().findViewById(R.id.photoPickerText)).setText("Изменить фотографию");
        }
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startPhotoPickerDialog();
            }
        }
    }

    private int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public void startPhotoPickerDialog() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        ImagePicker.pickImage(AutoRegistrationFragment.this, "Выберите фотографию:");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
