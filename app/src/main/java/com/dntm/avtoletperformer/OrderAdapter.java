package com.dntm.avtoletperformer;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class OrderAdapter extends ArrayAdapter<Order> {
    public OrderAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public OrderAdapter(Context context, int resource, List<Order> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.order_list_item, null);
        }

        Order order = getItem(position);

        TextView idOrder = (TextView) v.findViewById(R.id.idOrder);
        TextView startTime = (TextView) v.findViewById(R.id.startTimeOrder);
        TextView distanceRouteText = (TextView) v.findViewById(R.id.distanceRouteText);
        TextView workingDuration = (TextView) v.findViewById(R.id.workingDuration);
        LinearLayout totalFee = (LinearLayout) v.findViewById(R.id.totalFee);
        LinearLayout distanceRoute = (LinearLayout) v.findViewById(R.id.distanceRoute);
        TextView totalFeeText = (TextView) v.findViewById(R.id.totalFeeText);

        idOrder.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        startTime.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        distanceRouteText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        workingDuration.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        totalFeeText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));

        totalFee.setVisibility(order.getCanceled() ? View.GONE : View.VISIBLE);
        distanceRoute.setVisibility(Config.getInfoPerformer(getContext(),"type").equals(0) ? View.VISIBLE : View.GONE);

        idOrder.setText("Заказ №" + order.getId());

        if(order.getTimestampStart() > 0)
            startTime.setText(getDateCurrentTimeZone(order.getTimestampStart()));
        else
            startTime.setText("—");


        if(order.getWorkingDuration() > 60) {
            distanceRouteText.setText(order.getDistanceRoute() + " км");

            Long durationTimeHour = TimeUnit.SECONDS.toHours(order.getWorkingDuration());
            Long durationTimeMinute = TimeUnit.SECONDS.toMinutes(order.getWorkingDuration() - (durationTimeHour * 3600));

            String durationTime = durationTimeHour > 0 ?
                    String.format(Locale.US, "%s %s", getHourTimeString(durationTimeHour), getMinuteTimeString(durationTimeMinute)) :
                    String.format(Locale.US, "%s", getMinuteTimeString(durationTimeMinute));
            workingDuration.setText(durationTime);
        } else {
            workingDuration.setText("—");
            distanceRouteText.setText("—");
        }

        totalFeeText.setText(order.getTotalFee() + " \u20BD");

        return v;
    }

    private String getHourTimeString(long hour) {
        if(hour == 1)
            return "1 час";
        else if(hour > 1 && hour < 5)
            return hour + " часа";
        else if(hour >= 5)
            return hour + " часов";
        return "";
    }

    private String getMinuteTimeString(long minute) {
        if(minute == 1)
            return "1 минута";
        else if(minute > 1 && minute < 5)
            return minute + " минуты";
        else if(minute >= 5)
            return minute + " минут";
        return "";
    }

    private String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getTimeZone("UTC");
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) { }
        return "";
    }
}
