package com.dntm.avtoletperformer;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class ConfirmationOrderActivity extends AppCompatActivity {
    ProgressDialog pd;
    Integer orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation_order);
        setFinishOnTouchOutside(false);

        try {
            final Intent intent = getIntent();
            final JSONObject routeData = new JSONObject(intent.getStringExtra("routeData"));
            orderId = routeData.getInt("orderId");
            ((TextView) findViewById(R.id.originOrder)).setText(routeData.getString("origin"));
            ((TextView) findViewById(R.id.destinationOrder)).setText(routeData.getString("destination"));
            ((TextView) findViewById(R.id.cargoDescription)).setText(routeData.getString("cargoDescription"));
            ((TextView) findViewById(R.id.distanceToOrder)).setText(routeData.getString("distanceToOrder") + " км");
            ((TextView) findViewById(R.id.priceOrder)).setText(routeData.getString("price") + " \u20BD");

            if(!routeData.isNull("services") && Config.getInfoPerformer(this,"type").equals(0)) {
                String services = routeData.getString("services");
                ((LinearLayout) findViewById(R.id.servicesOrder)).setVisibility(View.VISIBLE);

                if(services.contains("1") && !services.contains("2"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Вывоз мусора");
                else if(services.contains("2") && !services.contains("1"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Экспедирование");
                else if(services.contains("1") && services.contains("2"))
                    ((TextView) findViewById(R.id.servicesOrderText)).setText("Вывоз мусора, экспедирование");
            }

            ((Button) findViewById(R.id.confirmOrder)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    pd = new ProgressDialog(ConfirmationOrderActivity.this);
                    pd.setMessage("Пожалуйста, подождите");
                    pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    pd.setIndeterminate(true);
                    pd.show();

                    if(getSharedPreferences("activeOrder", 0).getInt("orderId", -1) != -1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmationOrderActivity.this);
                        builder.setTitle("Уведомление")
                                .setMessage("У вас уже есть принятый заказ. Вы не можете принимать больше одного заказа.")
                                .setCancelable(false)
                                .setNegativeButton("ОК",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                finish();
                                            }
                                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                        return;
                    }

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                            Config.API_CONFIRM_ORDER + "?orderId=" + orderId + "&token=" + Config.getAuthToken(getApplicationContext()), null,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("ConfirmOrderResponse", response.toString());
                                    if (pd != null)
                                        pd.dismiss();
                                    try {
                                        if(response.getString("status").contains("error")) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(ConfirmationOrderActivity.this);
                                            builder.setTitle("Уведомление")
                                                    .setMessage(response.getString("errorMsg"))
                                                    .setCancelable(false)
                                                    .setNegativeButton("ОК",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                    finish();
                                                                }
                                                            });
                                            AlertDialog alert = builder.create();
                                            alert.show();
                                        } else if(response.getString("status").contains("success")) {
                                            Intent broadcast = new Intent(Config.PUSH_ORDER_CONFIRMED);
                                            LocalBroadcastManager.getInstance(ConfirmationOrderActivity.this).sendBroadcast(broadcast);

                                            Config.setInfoPerformer(ConfirmationOrderActivity.this,"status", 2);

                                            SharedPreferences.Editor activeOrder = getSharedPreferences("activeOrder", 0).edit();
                                            activeOrder.putInt("orderId", orderId);
                                            activeOrder.putString("performers", response.getJSONArray("performers").toString());
                                            activeOrder.putString("info", response.getJSONObject("info").toString());
                                            activeOrder.putString("routeData", response.getJSONObject("routeData").toString());
                                            activeOrder.putString("client", response.getJSONObject("client").toString());
                                            activeOrder.putInt("status", 0);
                                            activeOrder.apply();

                                            Intent activity = new Intent(ConfirmationOrderActivity.this, ActiveOrderActivity.class);
                                            startActivity(activity);
                                            finish();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ConfirmOrderResponse", error + "");
                        }
                    });
                    RequestQueue requestQueue = Volley.newRequestQueue(getBaseContext());
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                            0,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjectRequest);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Новый заказ");
        toolbar.setNavigationIcon(R.drawable.ic_close);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pd != null) {
            pd.dismiss();
            pd = null;
        }
    }
}
