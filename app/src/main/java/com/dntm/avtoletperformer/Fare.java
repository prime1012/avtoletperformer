package com.dntm.avtoletperformer;


public class Fare {
    private String name;
    private Integer minPrice;
    private Integer minTime;
    private Integer overTimePrice;
    private Integer overTime;
    private Integer intercityPrice;
    private Integer intercityDistance;
    private Integer highDemandAreaPrice;
    private Integer highDemandTimePrice;

    public Fare(String name, Integer minPrice, Integer minTime, Integer overTimePrice, Integer overTime,
                Integer intercityPrice, Integer intercityDistance, Integer highDemandAreaPrice, Integer highDemandTimePrice) {
        this.name = name;
        this.minPrice = minPrice;
        this.minTime = minTime;
        this.overTimePrice = overTimePrice;
        this.overTime = overTime;
        this.intercityPrice = intercityPrice;
        this.intercityDistance = intercityDistance;
        this.highDemandAreaPrice = highDemandAreaPrice;
        this.highDemandTimePrice = highDemandTimePrice;
    }

    public Integer getHighDemandAreaPrice() {
        return highDemandAreaPrice;
    }

    public Integer getHighDemandTimePrice() {
        return highDemandTimePrice;
    }

    public Double getIntercityDistance() {
        return Double.valueOf(intercityDistance) / 1000;
    }

    public Integer getIntercityPrice() {
        return intercityPrice;
    }

    public Integer getMinPrice() {
        return minPrice;
    }

    public Integer getMinTime() {
        return minTime;
    }

    public Integer getOverTime() {
        return overTime;
    }

    public Integer getOverTimePrice() {
        return overTimePrice;
    }

    public String getName() {
        return name;
    }
}
