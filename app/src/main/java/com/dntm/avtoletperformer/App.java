package com.dntm.avtoletperformer;

import android.app.Application;

import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder("b3d3c93a-33ec-48f3-9a79-9053418b6cd2");
        YandexMetrica.activate(getApplicationContext(), configBuilder.build());
        // Отслеживание активности пользователей
        YandexMetrica.enableActivityAutoTracking(this);
    }
}
