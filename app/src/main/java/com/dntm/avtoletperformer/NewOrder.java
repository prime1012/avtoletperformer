package com.dntm.avtoletperformer;

public class NewOrder {
    public Integer id;
    public String origin;
    public String destination;
    public String cargoDescription;
    public Double distanceToOrder;
    public Integer priceOrder;

    public NewOrder(Integer id, String origin, String destination, String cargoDescription, Double distanceToOrder, Integer priceOrder) {
        this.id = id;
        this.origin = origin;
        this.destination = destination;
        this.priceOrder = priceOrder;
        this.cargoDescription = cargoDescription;
        this.distanceToOrder = distanceToOrder;
    }

    public Integer getId() {
        return id;
    }

    public Double getDistanceToOrder() {
        return distanceToOrder;
    }

    public Integer getPriceOrder() {
        return priceOrder;
    }

    public String getCargoDescription() {
        return cargoDescription;
    }

    public String getDestination() {
        return destination;
    }

    public String getOrigin() {
        return origin;
    }
}
