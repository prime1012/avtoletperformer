package com.dntm.avtoletperformer;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class NewOrderAdapter extends ArrayAdapter<NewOrder> {
    public NewOrderAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public NewOrderAdapter(Context context, int resource, List<NewOrder> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.new_order_list_item, null);
        }

        NewOrder order = getItem(position);

        TextView idOrder = (TextView) v.findViewById(R.id.idOrder);
        TextView cargoDescription = (TextView) v.findViewById(R.id.cargoDescription);
        TextView distanceToOrder = (TextView) v.findViewById(R.id.distanceToOrder);
        TextView totalFeeOrder = (TextView) v.findViewById(R.id.totalFeeOrder);
        TextView priceOrder = (TextView) v.findViewById(R.id.priceOrder);

        idOrder.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        cargoDescription.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        distanceToOrder.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        priceOrder.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));

        idOrder.setText("Заказ №" + order.getId());
        cargoDescription.setText(order.getCargoDescription());
        distanceToOrder.setText(order.getDistanceToOrder() + " км");
        totalFeeOrder.setText(order.getPriceOrder() + " \u20BD");
        priceOrder.setText(order.getPriceOrder() + " \u20BD");

        return v;
    }
}
