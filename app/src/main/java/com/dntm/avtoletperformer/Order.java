package com.dntm.avtoletperformer;

public class Order {
    public Integer id;
    public Boolean isCanceled;
    public Long timestampStart;
    public Long timestampEnd;
    public Double distanceRoute;
    public Long workingDuration;
    public Integer totalFee;

    public Order(Integer id, Boolean isCanceled, Long timestampStart, Long timestampEnd, Double distanceRoute, Long workingDuration, Integer totalFee) {
        this.id = id;
        this.isCanceled = isCanceled;
        this.timestampStart = timestampStart;
        this.timestampEnd = timestampEnd;
        this.totalFee = totalFee;
        this.distanceRoute = distanceRoute;
        this.workingDuration = workingDuration;
    }

    public Integer getId() {
        return id;
    }

    public Integer getTotalFee() {
        return totalFee;
    }

    public Double getDistanceRoute() {
        return distanceRoute;
    }

    public Long getWorkingDuration() {
        return workingDuration;
    }

    public Long getTimestampEnd() {
        return timestampEnd;
    }

    public Long getTimestampStart() {
        return timestampStart;
    }

    public Boolean getCanceled() {
        return isCanceled;
    }
}
