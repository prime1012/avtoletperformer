package com.dntm.avtoletperformer;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.dntm.avtoletperformer.R;
import com.dntm.avtoletperformer.Performer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PerformerAdapter extends ArrayAdapter<Performer> {
    private Context mContext;

    public PerformerAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        mContext = context;
    }

    public PerformerAdapter(Context context, int resource, List<Performer> items) {
        super(context, resource, items);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.performer_list_item, null);
        }

        Performer p = getItem(position);

        Map<Integer, String> statusList = new HashMap<Integer, String>();
        statusList.put(2, "В пути");
        statusList.put(3, "По адресу");
        statusList.put(4, "Работает");
        statusList.put(5, "Освобождается");
        statusList.put(6, "Закончил работу");

        if (p != null) {
            ((LinearLayout) v.findViewById(R.id.profilePerformer)).setVisibility(View.VISIBLE);
            TextView profileName = (TextView) v.findViewById(R.id.profileName);
            TextView profileRating = (TextView) v.findViewById(R.id.profileRating);
            TextView profileStatus = (TextView) v.findViewById(R.id.profileStatus);
            final ImageView profilePhoto = (ImageView) v.findViewById(R.id.profilePhoto);
            Button profilePhone = (Button) v.findViewById(R.id.profilePhone);
            ImageView clerkIcon = (ImageView) v.findViewById(R.id.clerkType);
            ImageView driverIcon = (ImageView) v.findViewById(R.id.driverType);

            profileName.setText(p.getProfileName());
            profileRating.setText(p.getProfileRating().toString());
            profileStatus.setText(statusList.get(p.getProfileStatus()));

            if(p.getProfileStatus() != 6) {
                profilePhone.setVisibility(View.VISIBLE);
                profilePhone.setTag(p.getProfilePhone());
                profilePhone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ActivityCompat.checkSelfPermission(getContext(), android.Manifest.permission.CALL_PHONE)
                                == PackageManager.PERMISSION_GRANTED) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:+" + view.getTag().toString()));
                            getContext().startActivity(callIntent);
                        } else
                            ((ActiveOrderActivity) mContext).requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, 0);
                    }
                });
            }

            String url = Config.PROFILE_PHOTO_DRIVER + p.getProfileId() + "/photo_driver.jpg?token=" + Config.getAuthToken(getContext());
            if(p.getProfileType() == 1)
                url = Config.PROFILE_PHOTO_LOADER + p.getProfileId() + "/photo_loader.jpg?token=" + Config.getAuthToken(getContext());

            ImageRequest request = new ImageRequest(url,
                    new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap bitmap) {
                            int dimension = getSquareCropDimensionForBitmap(bitmap);
                            bitmap = ThumbnailUtils.extractThumbnail(bitmap, dimension, dimension);

                            RoundedBitmapDrawable roundedBmp = RoundedBitmapDrawableFactory.create(getContext().getResources(), bitmap);
                            roundedBmp.setCircular(true);
                            profilePhoto.setImageDrawable(roundedBmp);
                        }
                    }, 0, 0, null,
                    new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            Log.e("getProfilePhoto", error.toString());
                        }
                    });
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(request);

            if(p.getProfileType() == 0)
                driverIcon.setVisibility(View.VISIBLE);
            else if(p.getProfileType() == 1)
                clerkIcon.setVisibility(View.VISIBLE);

            profileName.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            profileRating.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
            profileStatus.setTypeface(ResourcesCompat.getFont(getContext(), R.font.cuprum_regular));
        } else
            ((LinearLayout) v.findViewById(R.id.searchPerformer)).setVisibility(View.VISIBLE);

        return v;
    }

    private int getSquareCropDimensionForBitmap(Bitmap bitmap) {
        //use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }
}
